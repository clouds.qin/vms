﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using VMS.Web.Models;

namespace VMS.Web.Filters
{
    public class ApiResultAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {

            //actionExecutedContext.Response.Headers.Add("content-type", "application/json");
            //actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK, actionExecutedContext.Response.Content.ReadAsAsync<string>().Result, "text/plain");
            //base.OnActionExecuted(actionExecutedContext);
            //return;
            ActionResult result = new ActionResult();
            if (actionExecutedContext.ActionContext.Response == null)
            {
                result.Code = HttpStatusCode.OK;
                result.Data = null;
            }
            else 
            {
                result.Code = actionExecutedContext.ActionContext.Response.StatusCode;
                result.Data = actionExecutedContext.Response.Content.ReadAsAsync<object>().Result;
            }
            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(result.Code, result, "application/json");
            //new HttpResponseMessage(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json");
            //base.OnActionExecuted(actionExecutedContext);
        }
    }
   
}