﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using NLog;

namespace VMS.Web.Filters
{
    public class ApiExceptionHandleAttribute : ExceptionFilterAttribute
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var result = new ServiceResult();
            if (actionExecutedContext.Exception is HttpException)
                result.Code = ((HttpException)actionExecutedContext.Exception).ErrorCode;
            else if (actionExecutedContext.Exception is HttpResponseException)
                result.Code = ((HttpResponseException)actionExecutedContext.Exception).Response.StatusCode.GetHashCode();
            else
                result.Code = 500;
            result.Exception = actionExecutedContext.Exception;
            //base.OnException(actionExecutedContext);
            _log.Error(actionExecutedContext.Exception);

            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK);
            actionExecutedContext.Response.Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json");
        }
    }
    public class ServiceResult
    {
        public int Code { get; set; }
        [JsonIgnore]
        public Exception Exception { get; set; }
        public String Message
        {
            get
            {
                return Exception.Message;
            }
        }
    }
}