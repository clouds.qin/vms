﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using VMS.Applications.IServices;
using VMS.Applications.Services;
using VMS.Repository;

namespace VMS.Web.App_Start
{
    public static class AutoFacConfig
    {
        public static ContainerBuilder builder;
        public static System.Web.Mvc.IDependencyResolver mvcResolver;
        public static System.Web.Http.Dependencies.IDependencyResolver apiResolver;

        public static void RegisterConfig()
        {
            builder = new ContainerBuilder();
            builder.RegisterType<EFDBContext>().As<IRepositoryContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(EFRepository<>)).As(typeof(IRepository<>));
            //  builder.RegisterFilterProvider();

            //Register controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).AsSelf().PropertiesAutowired();
            builder.RegisterType<AgentService>().As<IAgentService>().InstancePerLifetimeScope();
            builder.RegisterType<OrganizationService>().As<IOrganizationService>().InstancePerLifetimeScope();
            builder.RegisterType<SFDCOrgService>().As<ISFDCOrgService>().InstancePerLifetimeScope();
            builder.RegisterType<WelinkAccountService>().As<IWelinkAccountService>().InstancePerLifetimeScope();
            builder.RegisterType<CacheService>().As<ICacheService>().InstancePerLifetimeScope();
            builder.RegisterType<ComponentService>().As<IComponentService>().InstancePerLifetimeScope();
            builder.RegisterType<LicenseService>().As<ILicenseService>().InstancePerLifetimeScope();
            builder.RegisterType<LicenseTypeService>().As<ILicenseTypeService>().InstancePerLifetimeScope();
            builder.RegisterType<MessageFilterService>().As<IMessageFilterService>().InstancePerLifetimeScope();
            builder.RegisterType<MessageFilterKeywordService>().As<IMessageFilterKeywordService>().InstancePerLifetimeScope();
            builder.RegisterType<ProxyUserService>().As<IProxyUserService>().InstancePerLifetimeScope();
            builder.RegisterType<SuiteService>().As<ISuiteService>().InstancePerLifetimeScope();
            builder.RegisterType<SynchronouedQueueService>().As<ISynchronouedQueueService>().InstancePerLifetimeScope();
            builder.RegisterType<TemporaryRecordService>().As<ITemporaryRecordService>().InstancePerLifetimeScope();
            builder.RegisterType<WelinkAdoptionService>().As<IWelinkAdoptionService>().InstancePerLifetimeScope();
            builder.RegisterType<WelinkUserService>().As<IWelinkUserService>().InstancePerLifetimeScope();

            ///
            var container = builder.Build();
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            apiResolver = webApiResolver;
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;

            // Set the dependency resolver for MVC.
            var resolver = new AutofacDependencyResolver(container);
            mvcResolver = resolver;
            DependencyResolver.SetResolver(mvcResolver);
        }
    }
}