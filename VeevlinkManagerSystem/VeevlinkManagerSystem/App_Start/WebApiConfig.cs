﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using VMS.Web.Filters;

namespace VeevlinkManagerSystem
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
               routeTemplate: "v{ver}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, ver = "\\d+" }
            );
            config.Filters.Add(new ApiResultAttribute());
            config.Filters.Add(new ApiExceptionHandleAttribute());
        }
    }
}
