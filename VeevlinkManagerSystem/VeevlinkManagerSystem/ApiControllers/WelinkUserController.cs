﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/WelinkUser")]
    public class WelinkUserController : ApiController
    {
        private IWelinkUserService _welinkUserService;
        public WelinkUserController(IWelinkUserService welinkUserService)
        {
            _welinkUserService = welinkUserService;
        }

        [HttpGet, Route("GetWelinkUser")]

        public List<WelinkUserModel> GetWelinkUser(String keyWord = null, int page = 1)
        {
            return _welinkUserService.GetWelinkUser(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public WelinkUserModel AddWelinkUser(WelinkUserModel welinkUserModel)
        {
            WelinkUser welinkUser = new WelinkUser()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = welinkUserModel.Id,
                WelinkAccountId = welinkUserModel.WelinkAccountId,
                WechatUserID = welinkUserModel.WechatUserID,
                SFDCUserID = welinkUserModel.SFDCUserID,
                SFDCAccessToken = welinkUserModel.SFDCAccessToken,
                SFDCRefreshToken = welinkUserModel.SFDCRefreshToken,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                Active = welinkUserModel.Active,
                City = welinkUserModel.City,
                Country = welinkUserModel.Country,
                Email = welinkUserModel.Email,
                Language = welinkUserModel.Language,
                Mobile = welinkUserModel.Mobile,
                NickName = welinkUserModel.NickName,
                OpenID = welinkUserModel.OpenID,
                Position = welinkUserModel.Position,
                Province = welinkUserModel.Province,
                Sex = welinkUserModel.Sex,
                Status = welinkUserModel.Status,
                SubscribeTime = welinkUserModel.SubscribeTimeM,
                UnionID = welinkUserModel.UnionID,
                UniqueID = welinkUserModel.UniqueID,
                UnsubscribeTime = welinkUserModel.UnsubscribeTimeM,
                WechatID = welinkUserModel.WechatID,
                Type = welinkUserModel.Type,
                Subscribe = welinkUserModel.Subscribe,
                Name = welinkUserModel.Name,
                ProxyUserId = welinkUserModel.ProxyUserId,
                Syncon = welinkUserModel.SynconM,
            };
            return _welinkUserService.AddWelinkUser(welinkUser);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteWelinkUser(string id)
        {

            return _welinkUserService.DeleteWelinkUser(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateWelinkUser(WelinkUserModel welinkUserModel)
        {
            WelinkUser welinkUser = new WelinkUser()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = welinkUserModel.Id,
                WelinkAccountId = welinkUserModel.WelinkAccountId,
                WechatUserID = welinkUserModel.WechatUserID,
                SFDCUserID = welinkUserModel.SFDCUserID,
                SFDCAccessToken = welinkUserModel.SFDCAccessToken,
                SFDCRefreshToken = welinkUserModel.SFDCRefreshToken,
               // CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                Active = welinkUserModel.Active,
                City = welinkUserModel.City,
                Country = welinkUserModel.Country,
                Email = welinkUserModel.Email,
                Language = welinkUserModel.Language,
                Mobile = welinkUserModel.Mobile,
                NickName = welinkUserModel.NickName,
                OpenID = welinkUserModel.OpenID,
                Position = welinkUserModel.Position,
                Province = welinkUserModel.Province,
                Sex = welinkUserModel.Sex,
                Status = welinkUserModel.Status,
                SubscribeTime = welinkUserModel.SubscribeTimeM,
                UnionID = welinkUserModel.UnionID,
                UniqueID = welinkUserModel.UniqueID,
                UnsubscribeTime = welinkUserModel.UnsubscribeTimeM,
                WechatID = welinkUserModel.WechatID,
                Type = welinkUserModel.Type,
                Subscribe = welinkUserModel.Subscribe,
                Name = welinkUserModel.Name,
                ProxyUserId = welinkUserModel.ProxyUserId,
                Syncon = welinkUserModel.SynconM,
            };
            return _welinkUserService.UpdateWelinkUser(welinkUser);
        }
    }
}
