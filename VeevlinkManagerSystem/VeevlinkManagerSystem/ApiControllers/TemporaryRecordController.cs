﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/TemporaryRecord")]
    public class TemporaryRecordController : ApiController
    {
        private ITemporaryRecordService _temporaryRecordService;
        public TemporaryRecordController(ITemporaryRecordService temporaryRecordService)
        {
            _temporaryRecordService = temporaryRecordService;
        }

        [HttpGet, Route("GetTemporaryRecord")]

        public List<TemporaryRecordModel> GetTemporaryRecord(String keyWord = null, int page = 1)
        {
            return _temporaryRecordService.GetTemporaryRecord(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public TemporaryRecordModel AddTemporaryRecord(TemporaryRecordModel temporaryRecordModel)
        {
            TemporaryRecord temporaryRecord = new TemporaryRecord()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = temporaryRecordModel.Id,
                RecordId = temporaryRecordModel.RecordId,
                TableName = temporaryRecordModel.TableName,
                Type = temporaryRecordModel.Type,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow    
            };
            return _temporaryRecordService.AddTemporaryRecord(temporaryRecord);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteTemporaryRecord(string id)
        {

            return _temporaryRecordService.DeleteTemporaryRecord(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateTemporaryRecord(TemporaryRecordModel temporaryRecordModel)
        {
            TemporaryRecord temporaryRecord = new TemporaryRecord()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = temporaryRecordModel.Id,
                RecordId = temporaryRecordModel.RecordId,
                TableName = temporaryRecordModel.TableName,
                Type = temporaryRecordModel.Type,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _temporaryRecordService.UpdateTemporaryRecord(temporaryRecord);
        }
    }
}
