﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/Organization")]
    public class OrganizationController : ApiController
    {
            private IOrganizationService _organizationService;
            public OrganizationController(IOrganizationService organizationService)
            {
                 _organizationService = organizationService;
            }

            [HttpGet, Route("GetOrganization")]

            public List<OrganizationModel> GetAgent(String keyWord = null, int page = 1)
            {

                return _organizationService.GetOrganization(keyWord, page);
            }


            [HttpPost, Route("Create")]

            public OrganizationModel AddOrganization(OrganizationModel organizationModel)
            {
                Organization organization = new Organization()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = organizationModel.Name,
                    Active = organizationModel.Active,
                    SyncOn = organizationModel.SyncOnM,
                    CreatedOn = DateTime.UtcNow,
                    LastModifiedOn = DateTime.UtcNow
                };
                return _organizationService.AddOrganization(organization);
            }


            [HttpDelete, Route("Delete")]

            public Boolean DeleteOrganization(string id)
            {

                return _organizationService.DeleteOrganization(id);
            }

            [HttpPost, Route("Update")]

            public Boolean UpdateOrganization(OrganizationModel organizationModel)
            {
            Organization organization = new Organization()
            {
                Id = organizationModel.Id,
                Name = organizationModel.Name,
                Active = organizationModel.Active,
                SyncOn = organizationModel.SyncOnM,
                //CreatedOn = organizationModel.CreatedOn,
                LastModifiedOn = DateTime.UtcNow
            };
            return _organizationService.UpdateOrganization(organization);
            }
        }
    }