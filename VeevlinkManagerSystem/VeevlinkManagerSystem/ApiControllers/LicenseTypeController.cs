﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/LicenseType")]
    public class LicenseTypeController : ApiController
    {
        private ILicenseTypeService _licenseTypeService;
        public LicenseTypeController(ILicenseTypeService licenseTypeService)
        {
            _licenseTypeService = licenseTypeService;
        }

        [HttpGet, Route("GetLicenseType")]

        public List<LicenseTypeModel> GetLicenseType(String keyWord = null, int page = 1)
        {
            return _licenseTypeService.GetLicenseType(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public LicenseTypeModel AddLicenseType(LicenseTypeModel licenseTypeModel)
        {
            LicenseType licenseType = new LicenseType()
            {
                Id = Guid.NewGuid().ToString(),
                Name = licenseTypeModel.Name,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _licenseTypeService.AddLicenseType(licenseType);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteLicenseType(string id)
        {

            return _licenseTypeService.DeleteLicenseType(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateLicenseType(LicenseTypeModel licenseTypeModel)
        {
            LicenseType licenseType = new LicenseType()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = licenseTypeModel.Id,
                Name = licenseTypeModel.Name,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _licenseTypeService.UpdateLicenseType(licenseType);
        }
    }
}