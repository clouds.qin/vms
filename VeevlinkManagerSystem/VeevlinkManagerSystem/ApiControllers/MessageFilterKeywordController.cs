﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/MessageFilterKeyword")]
    public class MessageFilterKeywordController : ApiController
    {
        private IMessageFilterKeywordService _messageFilterKeywordService;
        public MessageFilterKeywordController(IMessageFilterKeywordService messageFilterKeywordService)
        {
            _messageFilterKeywordService = messageFilterKeywordService;
        }

        [HttpGet, Route("GetMessageFilterKeyword")]

        public List<MessageFilterKeywordModel> GetMessageFilterKeyword(String keyWord = null, int page = 1)
        {
            return _messageFilterKeywordService.GetMessageFilterKeyword(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public MessageFilterKeywordModel AddMessageFilterKeyword(MessageFilterKeywordModel messageFilterKeywordModel)
        {
            MessageFilterKeyword messageFilterKeyword = new MessageFilterKeyword()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = messageFilterKeywordModel.Id,
                MessageFilterId = messageFilterKeywordModel.MessageFilterId,
                Keyword = messageFilterKeywordModel.Keyword,
                Active = messageFilterKeywordModel.Active,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = messageFilterKeywordModel.SyncOnM
            };
            return _messageFilterKeywordService.AddMessageFilterKeyword(messageFilterKeyword);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteMessageFilterKeyword(string id)
        {

            return _messageFilterKeywordService.DeleteMessageFilterKeyword(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateMessageFilterKeyword(MessageFilterKeywordModel messageFilterKeywordModel)
        {
            MessageFilterKeyword messageFilterKeyword = new MessageFilterKeyword()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = messageFilterKeywordModel.Id,
                MessageFilterId = messageFilterKeywordModel.MessageFilterId,
                Keyword = messageFilterKeywordModel.Keyword,
                Active = messageFilterKeywordModel.Active,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = messageFilterKeywordModel.SyncOnM
            };
            return _messageFilterKeywordService.UpdateMessageFilterKeyword(messageFilterKeyword);
        }
    }
}