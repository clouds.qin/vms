﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/WelinkAccount")]
    public class WelinkAccountController : ApiController
    { 
        private IWelinkAccountService _welinkAccountService;
        public WelinkAccountController(IWelinkAccountService welinkAccountervice)
        {
            _welinkAccountService = welinkAccountervice;
        }

        [HttpGet, Route("GetwelinkAccount")]

        public List<WelinkAccountModel> GetwelinkAccount(String keyWord = null, int page = 1)
        {

            return _welinkAccountService.GetWelinkAccount(keyWord, page);
        }


        [HttpPost, Route("Create")]

        public WelinkAccountModel AddWelinkAccount(WelinkAccountModel welinkAccountModel)
        {
            WelinkAccount welinkAccount = new WelinkAccount()
            {
                Id = Guid.NewGuid().ToString(),
                OrganizationId = welinkAccountModel.OrganizationId,
                Name = welinkAccountModel.Name,
                Active = welinkAccountModel.Active,
                IsVeevlink = welinkAccountModel.IsVeevlink,
                LinkedSFDCOrgId = welinkAccountModel.LinkedSFDCOrgId,
                Type = welinkAccountModel.Type,
                SubType = welinkAccountModel.SubType,
                WechatAccountName = welinkAccountModel.WechatAccountName,
                WechatAccountID = welinkAccountModel.WechatAccountID,
                WechatAccessToken = welinkAccountModel.WechatAccessToken,
                IdentityMode = welinkAccountModel.IdentityMode,
                Channel = welinkAccountModel.Channel,
                AppID = welinkAccountModel.AppID,
                SourceID = welinkAccountModel.SourceID,
                AppSecret = welinkAccountModel.AppSecret,
                ExpiresIn = welinkAccountModel.ExpiresInM,
                EncodingAESKey = welinkAccountModel.EncodingAESKey,
                Token = welinkAccountModel.Token,
                LiveAgentButtonID = welinkAccountModel.LiveAgentButtonID,
                CalloutAdapter = welinkAccountModel.CalloutAdapter,
                CallinAdapter = welinkAccountModel.CallinAdapter,
                LiveAgentRouter = welinkAccountModel.LiveAgentRouter,
                LiveAgentChasitorInit = welinkAccountModel.LiveAgentChasitorInit,
                LiveAgentAssembly = welinkAccountModel.LiveAgentAssembly,
                SendMessageAdapter = welinkAccountModel.SendMessageAdapter,
                MainAssembly = welinkAccountModel.MainAssembly,
                CustomSetting = welinkAccountModel.CustomSetting,
                JSAPITicket = welinkAccountModel.JSAPITicket,
                EnableJSAPI = welinkAccountModel.EnableJSAPI,
                SFDCRefreshToken = welinkAccountModel.SFDCRefreshToken,
                SFDCUserId = welinkAccountModel.SFDCUserId,
                SFDCAccessToken = welinkAccountModel.SFDCAccessToken,
                AuthorizerRefreshToken = welinkAccountModel.AuthorizerRefreshToken,
                AuthorizerAccessToken = welinkAccountModel.AuthorizerAccessToken,
                ComponentId = welinkAccountModel.ComponentId,
                AuthorizationCode = welinkAccountModel.AuthorizationCode,
                AuthorizationState = welinkAccountModel.AuthorizationState,
                EnableLiveChat = welinkAccountModel.EnableLiveChat,
                ConnectionType = welinkAccountModel.ConnectionType,
                IVRRule = welinkAccountModel.IVRRule,
                EnableReverseProxy = welinkAccountModel.EnableReverseProxy,
                ShowDevError = welinkAccountModel.ShowDevError,
                EnableCommunity = welinkAccountModel.EnableCommunity,
                BackEndSystemType = welinkAccountModel.BackEndSystemType,
                LiveAgentQueueUrl = welinkAccountModel.LiveAgentQueueUrl,
                BaseAdapter = welinkAccountModel.BaseAdapter,
                EnableExternalQueue = welinkAccountModel.EnableExternalQueue,
                VeevChatQueueThreadNumber = welinkAccountModel.VeevChatQueueThreadNumber,
                HandlerQueueThreadNumber = welinkAccountModel.HandlerQueueThreadNumber,
                EnableMessageQueue = welinkAccountModel.EnableMessageQueue,
                EnableVeevChat = welinkAccountModel.EnableVeevChat,
                VeevChatQueueURL = welinkAccountModel.VeevChatQueueURL,
                HandlerQueueURL = welinkAccountModel.HandlerQueueURL,
                Syncon = welinkAccountModel.SynconM,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _welinkAccountService.AddWelinkAccount(welinkAccount);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteWelinkAccount(string id)
        {

            return _welinkAccountService.DeleteWelinkAccount(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateWelinkAccount(WelinkAccountModel welinkAccountModel)
        {
            WelinkAccount welinkAccount = new WelinkAccount()
            {
                Id = welinkAccountModel.Id,
                OrganizationId = welinkAccountModel.OrganizationId,
                Name = welinkAccountModel.Name,
                Active = welinkAccountModel.Active,
                IsVeevlink = welinkAccountModel.IsVeevlink,
                LinkedSFDCOrgId = welinkAccountModel.LinkedSFDCOrgId,
                Type = welinkAccountModel.Type,
                SubType = welinkAccountModel.SubType,
                WechatAccountName = welinkAccountModel.WechatAccountName,
                WechatAccountID = welinkAccountModel.WechatAccountID,
                WechatAccessToken = welinkAccountModel.WechatAccessToken,
                IdentityMode = welinkAccountModel.IdentityMode,
                Channel = welinkAccountModel.Channel,
                AppID = welinkAccountModel.AppID,
                SourceID = welinkAccountModel.SourceID,
                AppSecret = welinkAccountModel.AppSecret,
                ExpiresIn = welinkAccountModel.ExpiresInM,
                EncodingAESKey = welinkAccountModel.EncodingAESKey,
                Token = welinkAccountModel.Token,
                LiveAgentButtonID = welinkAccountModel.LiveAgentButtonID,
                CalloutAdapter = welinkAccountModel.CalloutAdapter,
                CallinAdapter = welinkAccountModel.CallinAdapter,
                LiveAgentRouter = welinkAccountModel.LiveAgentRouter,
                LiveAgentChasitorInit = welinkAccountModel.LiveAgentChasitorInit,
                LiveAgentAssembly = welinkAccountModel.LiveAgentAssembly,
                SendMessageAdapter = welinkAccountModel.SendMessageAdapter,
                MainAssembly = welinkAccountModel.MainAssembly,
                CustomSetting = welinkAccountModel.CustomSetting,
                JSAPITicket = welinkAccountModel.JSAPITicket,
                EnableJSAPI = welinkAccountModel.EnableJSAPI,
                SFDCRefreshToken = welinkAccountModel.SFDCRefreshToken,
                SFDCUserId = welinkAccountModel.SFDCUserId,
                SFDCAccessToken = welinkAccountModel.SFDCAccessToken,
                AuthorizerRefreshToken = welinkAccountModel.AuthorizerRefreshToken,
                AuthorizerAccessToken = welinkAccountModel.AuthorizerAccessToken,
                ComponentId = welinkAccountModel.ComponentId,
                AuthorizationCode = welinkAccountModel.AuthorizationCode,
                AuthorizationState = welinkAccountModel.AuthorizationState,
                EnableLiveChat = welinkAccountModel.EnableLiveChat,
                ConnectionType = welinkAccountModel.ConnectionType,
                IVRRule = welinkAccountModel.IVRRule,
                EnableReverseProxy = welinkAccountModel.EnableReverseProxy,
                ShowDevError = welinkAccountModel.ShowDevError,
                EnableCommunity = welinkAccountModel.EnableCommunity,
                BackEndSystemType = welinkAccountModel.BackEndSystemType,
                LiveAgentQueueUrl = welinkAccountModel.LiveAgentQueueUrl,
                BaseAdapter = welinkAccountModel.BaseAdapter,
                EnableExternalQueue = welinkAccountModel.EnableExternalQueue,
                VeevChatQueueThreadNumber = welinkAccountModel.VeevChatQueueThreadNumber,
                HandlerQueueThreadNumber = welinkAccountModel.HandlerQueueThreadNumber,
                EnableMessageQueue = welinkAccountModel.EnableMessageQueue,
                EnableVeevChat = welinkAccountModel.EnableVeevChat,
                VeevChatQueueURL = welinkAccountModel.VeevChatQueueURL,
                HandlerQueueURL = welinkAccountModel.HandlerQueueURL,
                Syncon = welinkAccountModel.SynconM,
              //  CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _welinkAccountService.UpdateWelinkAccount(welinkAccount);
        }
    }
}