﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/WelinkAdoption")]
    public class WelinkAdoptionController : ApiController
    {
        private IWelinkAdoptionService _welinkAdoptionService;
        public WelinkAdoptionController(IWelinkAdoptionService welinkAdoptionService)
        {
            _welinkAdoptionService = welinkAdoptionService;
        }

        [HttpGet, Route("GetWelinkAdoption")]

        public List<WelinkAdoptionModel> GetWelinkAdoption(String keyWord = null, int page = 1)
        {
            return _welinkAdoptionService.GetWelinkAdoption(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public WelinkAdoptionModel AddWelinkAdoption(WelinkAdoptionModel welinkAdoptionModel)
        {
            WelinkAdoption welinkAdoption = new WelinkAdoption()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = welinkAdoptionModel.Id,
                SFDCOrgId = welinkAdoptionModel.SFDCOrgId,
                OrganizationId = welinkAdoptionModel.OrganizationId,
                WelinkAccountId = welinkAdoptionModel.WelinkAccountId,
                WelinkUserId = welinkAdoptionModel.WelinkUserId,
                AgentId = welinkAdoptionModel.AgentId,
                EventType = welinkAdoptionModel.EventType,
                MsgType = welinkAdoptionModel.MsgType,
                EventKey = welinkAdoptionModel.EventKey,
                Action = welinkAdoptionModel.Action,
                ActionType = welinkAdoptionModel.ActionType,
                ActionTime = welinkAdoptionModel.ActionTimeM,
                FromUserID = welinkAdoptionModel.FromUserID,
                ToUserID = welinkAdoptionModel.ToUserID,
                CreatedOn = DateTime.UtcNow,
                Type = welinkAdoptionModel.Type,
                Content = welinkAdoptionModel.Content,
                MsgId = welinkAdoptionModel.MsgId,
                MediaId = welinkAdoptionModel.MediaId,
                MsgBody = welinkAdoptionModel.MsgBody,
                IsVeevlink = welinkAdoptionModel.IsVeevlink,
                IsLiveAgent = welinkAdoptionModel.IsLiveAgent,
                SFDCResponse = welinkAdoptionModel.SFDCResponse
            };
            return _welinkAdoptionService.AddWelinkAdoption(welinkAdoption);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteWelinkAdoption(string id)
        {

            return _welinkAdoptionService.DeleteWelinkAdoption(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateWelinkAdoption(WelinkAdoptionModel welinkAdoptionModel)
        {
            WelinkAdoption welinkAdoption = new WelinkAdoption()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = welinkAdoptionModel.Id,
                SFDCOrgId = welinkAdoptionModel.SFDCOrgId,
                OrganizationId = welinkAdoptionModel.OrganizationId,
                WelinkAccountId = welinkAdoptionModel.WelinkAccountId,
                WelinkUserId = welinkAdoptionModel.WelinkUserId,
                AgentId = welinkAdoptionModel.AgentId,
                EventType = welinkAdoptionModel.EventType,
                MsgType = welinkAdoptionModel.MsgType,
                EventKey = welinkAdoptionModel.EventKey,
                Action = welinkAdoptionModel.Action,
                ActionType = welinkAdoptionModel.ActionType,
                ActionTime = welinkAdoptionModel.ActionTimeM,
                FromUserID = welinkAdoptionModel.FromUserID,
                ToUserID = welinkAdoptionModel.ToUserID,
                //CreatedOn = DateTime.UtcNow,
                Type = welinkAdoptionModel.Type,
                Content = welinkAdoptionModel.Content,
                MsgId = welinkAdoptionModel.MsgId,
                MediaId = welinkAdoptionModel.MediaId,
                MsgBody = welinkAdoptionModel.MsgBody,
                IsVeevlink = welinkAdoptionModel.IsVeevlink,
                IsLiveAgent = welinkAdoptionModel.IsLiveAgent,
                SFDCResponse = welinkAdoptionModel.SFDCResponse
            };
            return _welinkAdoptionService.UpdateWelinkAdoption(welinkAdoption);
        }
    }
}
