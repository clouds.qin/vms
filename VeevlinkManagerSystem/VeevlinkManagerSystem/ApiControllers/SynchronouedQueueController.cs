﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/SynchronouedQueue")]
    public class SynchronouedQueueController : ApiController
    {
        private ISynchronouedQueueService _synchronouedQueueService;
        public SynchronouedQueueController(ISynchronouedQueueService synchronouedQueueService)
        {
            _synchronouedQueueService = synchronouedQueueService;
        }

        [HttpGet, Route("GetSynchronouedQueue")]

        public List<SynchronouedQueueModel> GetSynchronouedQueue(String keyWord = null, int page = 1)
        {
            return _synchronouedQueueService.GetSynchronouedQueue(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public SynchronouedQueueModel AddSynchronouedQueue(SynchronouedQueueModel synchronouedQueueModel)
        {
            SynchronouedQueue synchronouedQueue = new SynchronouedQueue()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = synchronouedQueueModel.Id,
                RecordId = synchronouedQueueModel.RecordId,
                TableName = synchronouedQueueModel.TableName,
                Type = synchronouedQueueModel.Type,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _synchronouedQueueService.AddSynchronouedQueue(synchronouedQueue);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteSynchronouedQueue(string id)
        {

            return _synchronouedQueueService.DeleteSynchronouedQueue(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateSynchronouedQueue(SynchronouedQueueModel synchronouedQueueModel)
        {
            SynchronouedQueue synchronouedQueue = new SynchronouedQueue()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = synchronouedQueueModel.Id,
                RecordId = synchronouedQueueModel.RecordId,
                TableName = synchronouedQueueModel.TableName,
                Type = synchronouedQueueModel.Type,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _synchronouedQueueService.UpdateSynchronouedQueue(synchronouedQueue);
        }
    }
}
