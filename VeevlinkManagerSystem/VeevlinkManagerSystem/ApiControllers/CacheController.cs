﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;
using Cache = VMS.Repository.Cache;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/Cache")]
    public class CacheController : ApiController
    {
        private ICacheService _cacheService;
        public CacheController(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        [HttpGet, Route("GetCache")]

        public List<CacheModel> GetCache(String keyWord = null, int page = 1)
        {
            return _cacheService.GetCache(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public CacheModel AddCache(CacheModel cacheModel)
        {
            Cache cache = new Cache()
            {
                Id = Guid.NewGuid().ToString(),
                WelinkUserId = cacheModel.WelinkUserId,
                Key = cacheModel.Key,
                Value = cacheModel.Value,
                Overdue = cacheModel.Overdue,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _cacheService.AddCache(cache);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteCache(string id)
        {

            return _cacheService.DeleteCache(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateCache(CacheModel cacheModel)
        {
            Cache cache = new Cache()
            {
                Id = cacheModel.Id,
                WelinkUserId = cacheModel.WelinkUserId,
                Key = cacheModel.Key,
                Value = cacheModel.Value,
                Overdue = cacheModel.Overdue,
               // CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _cacheService.UpdateCache(cache);
        }
    }
}