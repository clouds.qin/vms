﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/Suite")]
    public class SuiteController : ApiController
    {
        private ISuiteService _suiteService;
        public SuiteController(ISuiteService suiteService)
        {
            _suiteService = suiteService;
        }

        [HttpGet, Route("Getsuite")]

        public List<SuiteModel> Getsuite(String keyWord = null, int page = 1)
        {
            return _suiteService.Getsuite(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public SuiteModel Addsuite(SuiteModel suiteModel)
        {
            Suite suite = new Suite()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = suiteModel.Id,
                SuiteId = suiteModel.SuiteId,
                SuiteSecret = suiteModel.SuiteSecret,
                SuiteName = suiteModel.SuiteName,
                Token = suiteModel.Token,
                EncodingAESKey = suiteModel.EncodingAESKey,
                SuiteTicket = suiteModel.SuiteTicket,
                CallBackURL = suiteModel.CallBackURL,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _suiteService.Addsuite(suite);
        }


        [HttpDelete, Route("Delete")]

        public Boolean Deletesuite(string id)
        {

            return _suiteService.Deletesuite(id);
        }

        [HttpPost, Route("Update")]

        public Boolean Updatesuite(SuiteModel suiteModel)
        {
            Suite suite = new Suite()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = suiteModel.Id,
                SuiteId = suiteModel.SuiteId,
                SuiteSecret = suiteModel.SuiteSecret,
                SuiteName = suiteModel.SuiteName,
                Token = suiteModel.Token,
                EncodingAESKey = suiteModel.EncodingAESKey,
                SuiteTicket = suiteModel.SuiteTicket,
                CallBackURL = suiteModel.CallBackURL,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _suiteService.Updatesuite(suite);
        }
    }
}
