﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/Agent")]
    public class AgentController : ApiController
    {
        private IAgentService _agentService;
        public AgentController(IAgentService agentService)
        {
            _agentService = agentService;
        }

        [HttpGet, Route("GetAgent")]

        public List<AgentModel> GetAgent(String keyWord = null, int page = 1)
        {

            return _agentService.GetAgent(keyWord, page);
        }
       

        [HttpPost, Route("Create")]

        public AgentModel AddAgent(AgentModel agentModel)
        {
            Agent agent = new Agent()
            {
                Id = Guid.NewGuid().ToString(),
                WelinkAccountId = agentModel.WelinkAccountId,
                Name = agentModel.Name,
                Token = agentModel.Token,
                EncodingAESKey = agentModel.EncodingAESKey,
                AgentID = agentModel.AgentID,
                Active = agentModel.Active,
                Secret = agentModel.Secret,
                AccessToken = agentModel.AccessToken,
                SyncOn = agentModel.SyncOnM,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _agentService.AddAgent(agent);
        }

        [HttpDelete, Route("Delete")]

        public Boolean DeleteAgent(string id)
        {

            return _agentService.DeleteAgent(id);
        }

        [HttpPost, Route("Update")]

        public bool UpdateAgent(AgentModel agentModel)
        {
            Agent agent = new Agent()
            {
                Id = agentModel.Id,
                WelinkAccountId = agentModel.WelinkAccountId,
                Name = agentModel.Name,
                Token = agentModel.Token,
                EncodingAESKey = agentModel.EncodingAESKey,
                AgentID = agentModel.AgentID,
                //CreatedOn = agentModel.CreatedOn,
                LastModifiedOn = DateTime.UtcNow,
                Active = agentModel.Active,
                Secret = agentModel.Secret,
                AccessToken = agentModel.AccessToken,
                SyncOn = agentModel.SyncOnM
            };
            return _agentService.UpdateAgent(agent);
        }
    }
}