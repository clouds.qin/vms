﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/SFDCOrg")]
    public class SFDCOrgController : ApiController
    { 
        private ISFDCOrgService _sfdcOrgService;
        public SFDCOrgController(ISFDCOrgService sfdcOrgService)
        {
            _sfdcOrgService = sfdcOrgService;
        }

        [HttpGet, Route("GetSFDCOrg")]

        public List<SFDCOrgModel> GetSFDCOrg(String keyWord = null, int page = 1)
        {

            return _sfdcOrgService.GetSFDCOrg(keyWord, page);
        }


        [HttpPost, Route("Create")]

        public SFDCOrgModel AddSFDCOrg(SFDCOrgModel sfdcOrgModel)
        {
            SFDCOrg sfdcOrg = new SFDCOrg()
            {
                Id = Guid.NewGuid().ToString(),
                OrganizationId = sfdcOrgModel.OrganizationId,
                Name = sfdcOrgModel.Name,
                SFDCOrgID = sfdcOrgModel.SFDCOrgID,
                SFDCUserID = sfdcOrgModel.SFDCUserID,
                Type = sfdcOrgModel.Type,
                PublicSiteURL = sfdcOrgModel.PublicSiteURL,
                InstanceURL = sfdcOrgModel.InstanceURL,
                AccessToken = sfdcOrgModel.AccessToken,
                RefreshToken = sfdcOrgModel.RefreshToken,
                IssuedAt = sfdcOrgModel.IssuedAtM,
                ClientID = sfdcOrgModel.ClientID,
                ClientSecret = sfdcOrgModel.ClientSecret,
                LiveAgentURL = sfdcOrgModel.LiveAgentURL,
                LiveAgentDeploymentID = sfdcOrgModel.LiveAgentDeploymentID,
                LiveAgentButtonID = sfdcOrgModel.LiveAgentButtonID,
                Active = sfdcOrgModel.Active,
                NameSpace = sfdcOrgModel.NameSpace,
                ReverseProxyUrl = sfdcOrgModel.ReverseProxyUrl,
                SyncOn = sfdcOrgModel.SyncOnM,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _sfdcOrgService.AddSFDCOrg(sfdcOrg);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteSFDCOrg(string id)
        {

            return _sfdcOrgService.DeleteSFDCOrg(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateSFDCOrg(SFDCOrgModel sfdcOrgModel)
        {
            SFDCOrg sfdcOrg = new SFDCOrg()
            {
                Id = sfdcOrgModel.Id,
                OrganizationId = sfdcOrgModel.OrganizationId,
                Name = sfdcOrgModel.Name,
                SFDCOrgID = sfdcOrgModel.SFDCOrgID,
                SFDCUserID = sfdcOrgModel.SFDCUserID,
                Type = sfdcOrgModel.Type,
                PublicSiteURL = sfdcOrgModel.PublicSiteURL,
                InstanceURL = sfdcOrgModel.InstanceURL,
                AccessToken = sfdcOrgModel.AccessToken,
                RefreshToken = sfdcOrgModel.RefreshToken,
                IssuedAt = sfdcOrgModel.IssuedAtM,
                ClientID = sfdcOrgModel.ClientID,
                ClientSecret = sfdcOrgModel.ClientSecret,
                LiveAgentURL = sfdcOrgModel.LiveAgentURL,
                LiveAgentDeploymentID = sfdcOrgModel.LiveAgentDeploymentID,
                LiveAgentButtonID = sfdcOrgModel.LiveAgentButtonID,
                Active = sfdcOrgModel.Active,
                NameSpace = sfdcOrgModel.NameSpace,
                ReverseProxyUrl = sfdcOrgModel.ReverseProxyUrl,
                SyncOn = sfdcOrgModel.SyncOnM,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _sfdcOrgService.UpdateSFDCOrg(sfdcOrg);
        }
    }
    }
