﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/ProxyUser")]
    public class ProxyUserController : ApiController
    {
        private IProxyUserService _proxyUserService;
        public ProxyUserController(IProxyUserService proxyUserService)
        {
            _proxyUserService = proxyUserService;
        }

        [HttpGet, Route("GetProxyUser")]

        public List<ProxyUserModel> GetProxyUser(String date = null, int page = 1)
        {
            return _proxyUserService.GetProxyUser(date, page);
        }

        [HttpPost, Route("Create")]

        public ProxyUserModel AddProxyUser(ProxyUserModel proxyUserModel)
        {
            ProxyUser proxyUser = new ProxyUser()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = proxyUserModel.Id,
                SFDCOrgId = proxyUserModel.SFDCOrgId,
                WelinkAccountId = proxyUserModel.WelinkAccountId,
                SFDCUserID = proxyUserModel.SFDCUserID,
                AccessToken = proxyUserModel.AccessToken,
                RefreshToken = proxyUserModel.RefreshToken,
                Active = proxyUserModel.Active,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = proxyUserModel.SyncOnM
        };
            return _proxyUserService.AddProxyUser(proxyUser);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeletProxyUser(string id)
        {

            return _proxyUserService.DeletProxyUser(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateProxyUser(ProxyUserModel proxyUserModel)
        {
            ProxyUser proxyUser = new ProxyUser()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = proxyUserModel.Id,
                SFDCOrgId = proxyUserModel.SFDCOrgId,
                WelinkAccountId = proxyUserModel.WelinkAccountId,
                SFDCUserID = proxyUserModel.SFDCUserID,
                AccessToken = proxyUserModel.AccessToken,
                RefreshToken = proxyUserModel.RefreshToken,
                Active = proxyUserModel.Active,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = proxyUserModel.SyncOnM
            };
            return _proxyUserService.UpdateProxyUser(proxyUser);
        }
    }
}