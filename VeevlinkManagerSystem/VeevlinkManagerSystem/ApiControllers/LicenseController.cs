﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/License")]
    public class LicenseController : ApiController
    {
        private ILicenseService _licenseService;
        public LicenseController(ILicenseService licenseService)
        {
            _licenseService = licenseService;
        }

        [HttpGet, Route("GetLicense")]

        public List<LicenseModel> GetLicense(String keyWord = null, int page = 1)
        {
            return _licenseService.GetLicense(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public LicenseModel AddLicenset(LicenseModel licenseModel)
        {
            License license = new License()
            {
                //Id = licenseModel.Id,
                Id = Guid.NewGuid().ToString(),
                Name = licenseModel.Name,
                OrganizationId = licenseModel.OrganizationId,
                LicenseTypeId = licenseModel.LicenseTypeId,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _licenseService.AddLicense(license);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteLicense(string id)
        {

            return _licenseService.DeleteLicense(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateLicense(LicenseModel licenseModel)
        {
            License license = new License()
            {
                Id = licenseModel.Id,
                //Id = Guid.NewGuid().ToString(),
                Name = licenseModel.Name,
                OrganizationId = licenseModel.OrganizationId,
                LicenseTypeId = licenseModel.LicenseTypeId,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow
            };
            return _licenseService.UpdateLicense(license);
        }
    }
}