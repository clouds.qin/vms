﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/Component")]
    public class ComponentController : ApiController
    {
        private IComponentService _componentService;
        public ComponentController(IComponentService componentService)
        {
            _componentService = componentService;
        }

        [HttpGet, Route("GetComponent")]

        public List<ComponentModel> GetComponent(String keyWord = null, int page = 1)
        {
            return _componentService.GetComponent(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public ComponentModel AddComponent(ComponentModel componentModel)
        {
            Component component = new Component()
            {
                Id = Guid.NewGuid().ToString(),
                Name = componentModel.Name,
                AppId = componentModel.AppId,
                AppSecret = componentModel.AppSecret,
                VerifyTicket = componentModel.VerifyTicket,
                AccessToken = componentModel.AccessToken,
                Token = componentModel.Token,
                EncodingAESKey = componentModel.EncodingAESKey,
                Active = componentModel.Active,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = componentModel.SyncOnM
        };
            return _componentService.AddComponent(component);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteComponent(string id)
        {

            return _componentService.DeleteComponent(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateComponent(ComponentModel componentModel)
        {
            Component component = new Component()
            {
                Id = componentModel.Id,
                Name = componentModel.Name,
                AppId = componentModel.AppId,
                AppSecret = componentModel.AppSecret,
                VerifyTicket = componentModel.VerifyTicket,
                AccessToken = componentModel.AccessToken,
                Token = componentModel.Token,
                EncodingAESKey = componentModel.EncodingAESKey,
                Active = componentModel.Active,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = componentModel.SyncOnM
            };
            return _componentService.UpdateComponent(component);
        }
    }
}