﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Web.ApiControllers
{
    [RoutePrefix("api/v1/MessageFilter")]
    public class MessageFilterController : ApiController
    {
        private IMessageFilterService _messageFilterService;
        public MessageFilterController(IMessageFilterService messageFilterService)
        {
            _messageFilterService = messageFilterService;
        }

        [HttpGet, Route("GetMessageFilter")]

        public List<MessageFilterModel> GetMessageFilter(String keyWord = null, int page = 1)
        {
            return _messageFilterService.GetMessageFilter(keyWord, page);
        }

        [HttpPost, Route("Create")]

        public MessageFilterModel AddMessageFilter(MessageFilterModel messageFilterModel)
        {
            MessageFilter messageFilter = new MessageFilter()
            {
                Id = Guid.NewGuid().ToString(),
                //Id = messageFilter.Id,
                WelinkAccountId = messageFilterModel.WelinkAccountId,
                MessageType = messageFilterModel.MessageType,
                AsynForward = messageFilterModel.AsynForward,
                Active = messageFilterModel.Active,
                CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = messageFilterModel.SyncOnM,
                EventType = messageFilterModel.EventType
            };
            return _messageFilterService.AddMessageFilter(messageFilter);
        }


        [HttpDelete, Route("Delete")]

        public Boolean DeleteMessageFilter(string id)
        {

            return _messageFilterService.DeleteMessageFilter(id);
        }

        [HttpPost, Route("Update")]

        public Boolean UpdateMessageFilter(MessageFilterModel messageFilterModel)
        {
            MessageFilter messageFilter = new MessageFilter()
            {
                //Id = Guid.NewGuid().ToString(),
                Id = messageFilterModel.Id,
                WelinkAccountId = messageFilterModel.WelinkAccountId,
                MessageType = messageFilterModel.MessageType,
                AsynForward = messageFilterModel.AsynForward,
                Active = messageFilterModel.Active,
                //CreatedOn = DateTime.UtcNow,
                LastModifiedOn = DateTime.UtcNow,
                SyncOn = messageFilterModel.SyncOnM,
                EventType = messageFilterModel.EventType
            };
            return _messageFilterService.UpdateMessageFilter(messageFilter);
        }
    }
}