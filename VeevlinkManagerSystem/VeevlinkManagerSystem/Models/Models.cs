﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;

namespace VMS.Web.Models
{
    public class ServiceResult
    {
        public int Code { get; set; }
        [JsonIgnore]
        public Exception Exception { get; set; }
        public String ErrorMessage
        {
            get
            {
                return Exception.Message;
            }
        }
    }
    public class ActionResult
    {
        public object Data { get; set; }
        public HttpStatusCode Code { get; set; }
        public string ErrorMessage { get; set; }
    }
}