﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Applications
{
    public class BusinessDataOrgResult
    {
        // CreatedOn = DateTime.Now.ToLocalTime().toString();//获取当前时间
        public DateTime? CreatedOn { get { return CreatedOn; } set { CreatedOn = DateTime.Now.ToLocalTime(); } }
        public DateTime? LastModifiedOn { get { return LastModifiedOn; } set { LastModifiedOn = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); } }
    }
}
