﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ILicenseService
    {
        //根据Name关键字查询并分页
        List<LicenseModel> GetLicense(string keyWord, int page);
        //增加
        LicenseModel AddLicense(License license);
        //删除
        Boolean DeleteLicense(string id);
        //修改
        Boolean UpdateLicense(License license);
    }
}
