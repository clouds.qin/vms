﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IMessageFilterService
    {
        //根据MessageType关键字查询并分页
        List<MessageFilterModel> GetMessageFilter(string keyWord, int page);
        //增加
        MessageFilterModel AddMessageFilter(MessageFilter messageFilter);
        //删除
        Boolean DeleteMessageFilter(string id);
        //修改
        Boolean UpdateMessageFilter(MessageFilter messageFilter);
    }
}
