﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ISFDCOrgService
    {
        //根据Name关键字查询并分页
        List<SFDCOrgModel> GetSFDCOrg(string keyWord, int page);
        //增加
        SFDCOrgModel AddSFDCOrg(SFDCOrg sfdcOrg);
        //删除
        Boolean DeleteSFDCOrg(string id);
        //修改
        Boolean UpdateSFDCOrg(SFDCOrg sfdcOrg);
    }
}
