﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IOrganizationService
    {
        //根据Name关键字查询并分页
        List<OrganizationModel> GetOrganization(string keyWord, int page);
        //增加
        OrganizationModel AddOrganization(Organization organization);
        //删除
        Boolean DeleteOrganization(string id);
        //修改
        Boolean UpdateOrganization(Organization organization);
    }
}
