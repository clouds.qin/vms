﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ILicenseTypeService
    {
        //根据Name关键字查询并分页
        List<LicenseTypeModel> GetLicenseType(string keyWord, int page);
        //增加
        LicenseTypeModel AddLicenseType(LicenseType licenseType);
        //删除
        Boolean DeleteLicenseType(string id);
        //修改
        Boolean UpdateLicenseType(LicenseType licenseType);
    }
}
