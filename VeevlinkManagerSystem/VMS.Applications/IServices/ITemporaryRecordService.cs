﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ITemporaryRecordService
    {
        //根据TableName关键字查询并分页
        List<TemporaryRecordModel> GetTemporaryRecord(string keyWord, int page);
        //增加
        TemporaryRecordModel AddTemporaryRecord(TemporaryRecord temporaryRecord);
        //删除
        Boolean DeleteTemporaryRecord(string id);
        //修改
        Boolean UpdateTemporaryRecord(TemporaryRecord temporaryRecord);
    }
}
