﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IMessageFilterKeywordService
    {

        //根据KeyWord关键字查询并分页
        List<MessageFilterKeywordModel> GetMessageFilterKeyword(string keyWord, int page);
        //增加
        MessageFilterKeywordModel AddMessageFilterKeyword(MessageFilterKeyword messageFilterKeyword);
        //删除
        Boolean DeleteMessageFilterKeyword(string id);
        //修改
        Boolean UpdateMessageFilterKeyword(MessageFilterKeyword messageFilterKeyword);
    }
}
