﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ISuiteService
    {
        //根据SuiteName关键字查询并分页
        List<SuiteModel> Getsuite(string keyWord, int page);
        //增加
        SuiteModel Addsuite(Suite suite);
        //删除
        Boolean Deletesuite(string id);
        //修改
        Boolean Updatesuite(Suite suite);
    }
}
