﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IWelinkUserService
    {
        //根据NickName关键字查询并分页
        List<WelinkUserModel> GetWelinkUser(string keyWord, int page);
        //增加
        WelinkUserModel AddWelinkUser(WelinkUser welinkUser);
        //删除
        Boolean DeleteWelinkUser(string id);
        //修改
        Boolean UpdateWelinkUser(WelinkUser welinkUser);
    }
}
