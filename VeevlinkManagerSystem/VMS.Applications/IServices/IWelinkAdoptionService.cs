﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IWelinkAdoptionService
    {
        //根据ActionType关键字查询并分页
        List<WelinkAdoptionModel> GetWelinkAdoption(string keyWord, int page);
        //增加
        WelinkAdoptionModel AddWelinkAdoption(WelinkAdoption welinkAdoption);
        //删除
        Boolean DeleteWelinkAdoption(string id);
        //修改
        Boolean UpdateWelinkAdoption(WelinkAdoption welinkAdoption);
    }
}
