﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IComponentService
    {
        //根据Name关键字查询并分页
        List<ComponentModel> GetComponent(string keyWord, int page);
        //增加
        ComponentModel AddComponent(Component component);
        //删除
        Boolean DeleteComponent(string id);
        //修改
        Boolean UpdateComponent(Component component);
    }
}
