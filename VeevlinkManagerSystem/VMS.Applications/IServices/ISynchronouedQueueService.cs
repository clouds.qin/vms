﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ISynchronouedQueueService
    {
        //根据TableName关键字查询并分页
        List<SynchronouedQueueModel> GetSynchronouedQueue(string keyWord, int page);
        //增加
        SynchronouedQueueModel AddSynchronouedQueue(SynchronouedQueue synchronouedQueue);
        //删除
        Boolean DeleteSynchronouedQueue(string id);
        //修改
        Boolean UpdateSynchronouedQueue(SynchronouedQueue synchronouedQueue);
    }
}
