﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IWelinkAccountService 
    {
        //根据Name关键字查询并分页
        List<WelinkAccountModel> GetWelinkAccount(string keyWord, int page);
        //增加
        WelinkAccountModel AddWelinkAccount(WelinkAccount welinkAccount);
        //删除
        Boolean DeleteWelinkAccount(string id);
        //修改
        Boolean UpdateWelinkAccount(WelinkAccount welinkAccount);
    }
}
