﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IProxyUserService
    {
        //根据关键字查询并分页
        List<ProxyUserModel> GetProxyUser(string date, int page);
        //增加
        ProxyUserModel AddProxyUser(ProxyUser proxyUser);
        //删除
        Boolean DeletProxyUser(string id);
        //修改
        Boolean UpdateProxyUser(ProxyUser proxyUser);
    }
}
