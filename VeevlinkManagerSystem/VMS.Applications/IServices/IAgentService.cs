﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface IAgentService
    {
        //根据Name关键字查询并分页
        List<AgentModel> GetAgent(string keyWord, int page);
        //增加
        AgentModel AddAgent(Agent agent);
        //删除
        Boolean DeleteAgent(string id);
        //修改
        Boolean UpdateAgent(Agent agent);
    }
}
