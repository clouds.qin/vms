﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.IServices
{
    public interface ICacheService
    {
        //根据key关键字查询并分页
        List<CacheModel> GetCache(string keyWord, int page);
        //增加
        CacheModel AddCache(Cache cache);
        //删除
        Boolean DeleteCache(string id);
        //修改
        Boolean UpdateCache(Cache cache);
    }
}
