﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class WelinkAdoptionService : IWelinkAdoptionService
    {
        private IRepository<WelinkAdoption> _welinkAdoption;
        public WelinkAdoptionService(IRepository<WelinkAdoption> welinkAdoption)
        {
            _welinkAdoption = welinkAdoption;
        }

        public WelinkAdoptionModel AddWelinkAdoption(WelinkAdoption welinkAdoption)
        {
            _welinkAdoption.Create(welinkAdoption);
            WelinkAdoptionModel welinkAdoptionModel = new WelinkAdoptionModel(welinkAdoption);
            return welinkAdoptionModel;
        }

        public bool DeleteWelinkAdoption(string id)
        {
            var welinkAdoption = _welinkAdoption.All.FirstOrDefault(r => r.Id == id);
            if (welinkAdoption != null)
            {
                _welinkAdoption.Delete(welinkAdoption);
                // welinkAdoption.Active = 0;
                UpdateWelinkAdoption(welinkAdoption);
            }

            return welinkAdoption != null;
        }

        public List<WelinkAdoptionModel> GetWelinkAdoption(string keyWord, int page)
        {
            WelinkAdoptionModel welinkAdoptionModel;
            var Modellist = new List<WelinkAdoptionModel>();
            int pageSize = 10;
            List<WelinkAdoption> list = new List<WelinkAdoption>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _welinkAdoption.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _suite.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _welinkAdoption.All.Where(p => p.ActionType.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _suite.All.Where(p => p.SuiteName.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var welinkAdoption in list)
            {
                welinkAdoptionModel = new WelinkAdoptionModel(welinkAdoption);
                Modellist.Add(welinkAdoptionModel);
            }

            return Modellist;
        }

        public Boolean UpdateWelinkAdoption(WelinkAdoption welinkAdoption)
        {
            var welinkAdoptionDB = _welinkAdoption.All.FirstOrDefault(r => r.Id == welinkAdoption.Id);
            if (welinkAdoptionDB != null)
            {
                welinkAdoption.CreatedOn = welinkAdoptionDB.CreatedOn;
                // WelinkAdoptionModel welinkAdoptionModel = new WelinkAdoptionModel(welinkAdoption);
                _welinkAdoption.Update(welinkAdoption);
            }
            return welinkAdoptionDB != null;
        }
    }
}


