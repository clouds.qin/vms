﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class MessageFilterService : IMessageFilterService
    {
        private IRepository<MessageFilter> _messageFilter;
        public MessageFilterService(IRepository<MessageFilter> messageFilter)
        {
            _messageFilter = messageFilter;
        }

        public MessageFilterModel AddMessageFilter(MessageFilter messageFilter)
        {
            _messageFilter.Create(messageFilter);
            MessageFilterModel messageFilterModel = new MessageFilterModel(messageFilter);
            return messageFilterModel;
        }

        public bool DeleteMessageFilter(string id)
        {
            var messageFilter = _messageFilter.All.FirstOrDefault(r => r.Id == id);
            if (messageFilter != null)
            {
                //_messageFilter.Delete(messageFilter);
                messageFilter.Active = 0;
                UpdateMessageFilter(messageFilter);
            }

            return messageFilter != null;
        }

        public List<MessageFilterModel> GetMessageFilter(string keyWord, int page)
        {
            MessageFilterModel messageFilterModel;
            var Modellist = new List<MessageFilterModel>();
            int pageSize = 10;
            List<MessageFilter> list = new List<MessageFilter>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _messageFilter.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _licenseType.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _messageFilter.All.Where(p => p.MessageType.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _licenseType.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var messageFilter in list)
            {
                messageFilterModel = new MessageFilterModel(messageFilter);
                Modellist.Add(messageFilterModel);
            }

            return Modellist;
        }

        public Boolean UpdateMessageFilter(MessageFilter messageFilter)
        {
            var messageFilterDB = _messageFilter.All.FirstOrDefault(r => r.Id == messageFilter.Id);
            if (messageFilterDB != null)
            {
                messageFilter.CreatedOn = messageFilterDB.CreatedOn;
                // MessageFilterModel messageFilterModel = new MessageFilterModel(messageFilter);
                _messageFilter.Update(messageFilter);
            }
            return messageFilterDB != null;
        }
    }
}
