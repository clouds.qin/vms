﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class WelinkAccountService : IWelinkAccountService
    {
        private IRepository<Agent> _agent;
        private IRepository<Organization> _organization;
        private IRepository<SFDCOrg> _sfdcOrg;
        private IRepository<WelinkAccount> _welinkAccount;
        public WelinkAccountService(IRepository<Agent> agent, IRepository<Organization> organization, IRepository<SFDCOrg> sfdcOrg, IRepository<WelinkAccount> welinkAccount)
        {
            _agent = agent;
            _organization = organization;
            _sfdcOrg = sfdcOrg;
            _welinkAccount = welinkAccount;
        }

        public WelinkAccountModel AddWelinkAccount(WelinkAccount welinkAccount)
        {
            _welinkAccount.Create(welinkAccount);
            WelinkAccountModel welinkAccountModel = new WelinkAccountModel(welinkAccount);
            return welinkAccountModel;
        }

        public bool DeleteWelinkAccount(string id)
        {
            var welinkAccount = _welinkAccount.All.FirstOrDefault(r => r.Id == id);
            if (welinkAccount != null)
            {
                // _welinkAccount.Delete(welinkAccount);
                welinkAccount.Active = 0;
                UpdateWelinkAccount(welinkAccount);
            }

            return welinkAccount != null;
        }

        public List<WelinkAccountModel> GetWelinkAccount(string keyWord, int page)
        {
            WelinkAccountModel welinkAccountModel;
            var Modellist = new List<WelinkAccountModel>();
            int pageSize = 10;
            List<WelinkAccount> list = new List<WelinkAccount>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _welinkAccount.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _welinkAccount.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _welinkAccount.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _welinkAccount.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var welinkAccount in list)
            {
                welinkAccountModel = new WelinkAccountModel(welinkAccount);
                Modellist.Add(welinkAccountModel);
            }

            return Modellist;
        }

        public Boolean UpdateWelinkAccount(WelinkAccount welinkAccount)
        {
            var welinkAccountDB = _welinkAccount.All.FirstOrDefault(r => r.Id == welinkAccount.Id);
            if (welinkAccountDB != null)
            {
                welinkAccount.CreatedOn = welinkAccountDB.CreatedOn;
               // WelinkAccountModel welinkAccountModel = new WelinkAccountModel(welinkAccount);
                _welinkAccount.Update(welinkAccount);
            }
            
            return welinkAccountDB != null;
        }
    }
}
