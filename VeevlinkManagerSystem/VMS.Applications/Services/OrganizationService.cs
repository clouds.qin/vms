﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class OrganizationService : IOrganizationService
    {
        private IRepository<Agent> _agent;
        private IRepository<Organization> _organization;
        private IRepository<SFDCOrg> _sfdcOrg;
        private IRepository<WelinkAccount> _welinkAccount;
        public OrganizationService(IRepository<Agent> agent, IRepository<Organization> organization, IRepository<SFDCOrg> sfdcOrg, IRepository<WelinkAccount> welinkAccount)
        {
            _agent = agent;
            _organization = organization;
            _sfdcOrg = sfdcOrg;
            _welinkAccount = welinkAccount;
        }

        /// <summary>
        /// OrganizationModel AddOrganization(Organization organization)：添加企业微信应用Agent
        /// </summary>
        /// <param name="agent">Organization实体</param>
        /// <returns>AgentModel</returns>
        public OrganizationModel AddOrganization(Organization organization)
        {
            _organization.Create(organization);
            OrganizationModel organizationModel = new OrganizationModel(organization);
            return organizationModel;
        }

        /// <summary>
        /// bool  DeleteOrganization(string id)：将用户organization的id实体的active改为inActive，不被启用状态
        /// </summary>
        /// <param name="id">organization的id</param>
        /// <returns>bool</returns>
        public bool DeleteOrganization(string id)
        {
            var organization = _organization.All.FirstOrDefault(r => r.Id == id);
            if (organization != null)
            {
                // _organization.Delete(agent);
                organization.Active = 0;
                UpdateOrganization(organization);
            }

            return organization != null;
        }


        /// <summary>
        /// List<OrganizationModel> GetOrganization:根据Name关键字查询客户Organization
        /// </summary>
        /// <param name="keyWord">Name关键字</param>
        /// <param name="page">页码数</param>
        /// <returns> List<OrganizationModel></returns>
        public List<OrganizationModel> GetOrganization(string keyWord, int page)
        {
            OrganizationModel organizationModel;
            var Modellist = new List<OrganizationModel>();
            int pageSize = 10;
            List<Organization> list = new List<Organization>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _organization.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _organization.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _organization.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
               // list = _organization.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var organization in list)
            {
                organizationModel = new OrganizationModel(organization);
                Modellist.Add(organizationModel);
            }

            return Modellist;
        }

        /// <summary>
        /// OrganizationModel UpdateOrganization(Organization agent):更新客户Organization
        /// </summary>
        /// <param name="agent">客户Organization实体</param>
        /// <returns>OrganizationModel</returns>
        public bool UpdateOrganization(Organization organization)
        {
            var organizationDB = _organization.All.FirstOrDefault(r => r.Id == organization.Id);
            if (organizationDB != null)
            {
                organization.CreatedOn = organizationDB.CreatedOn;
               // OrganizationModel organizationModel = new OrganizationModel(organization);
                _organization.Update(organization);
            }
           
            return organizationDB != null;
        }
    }
}
