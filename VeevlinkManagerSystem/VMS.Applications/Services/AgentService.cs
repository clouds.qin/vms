﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class AgentService : IAgentService
    {
        private IRepository<Agent> _agent;
        private IRepository<Organization> _organization;
        private IRepository<SFDCOrg> _sfdcOrg;
        private IRepository<WelinkAccount> _welinkAccount;
        public AgentService(IRepository<Agent> agent, IRepository<Organization> organization, IRepository<SFDCOrg> sfdcOrg, IRepository<WelinkAccount> welinkAccount)
        {
            _agent = agent;
            _organization = organization;
            _sfdcOrg = sfdcOrg;
            _welinkAccount = welinkAccount;
        }
        /// <summary>
        /// AgentModel AddAgent(Agent agent)：添加企业微信应用Agent
        /// </summary>
        /// <param name="agent">Agent实体</param>
        /// <returns>AgentModel</returns>
        public AgentModel AddAgent(Agent agent)
        {
            _agent.Create(agent);
            AgentModel agentModel = new AgentModel(agent);
            return agentModel;
        }

        /// <summary>
        /// bool DeleteAgent(string id)：将微信应用Agent实体的active改为inActive，不被启用状态
        /// </summary>
        /// <param name="id">Agent的id</param>
        /// <returns>bool</returns>
        public bool DeleteAgent(string id)
        {
            var agent = _agent.All.FirstOrDefault(r => r.Id == id);
            if (agent != null)
            {
                // _agent.Delete(agent);
                agent.Active = 0;
                UpdateAgent(agent);
            }

            return agent != null;
        }

        /// <summary>
        ///  List<AgentModel> GetAgent:根据Name关键字查询企业微信应用Agent
        /// </summary>
        /// <param name="keyWord">Name关键字</param>
        /// <param name="page">页码数</param>
        /// <returns>List<AgentModel></returns>
        public List<AgentModel> GetAgent(string keyWord, int page)
        {
            AgentModel agentModel;
            var Modellist = new List<AgentModel>();
            int pageSize = 10;
            List<Agent> list = new List<Agent>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _agent.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
               // list = _agent.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            }
            else
            {
                list = _agent.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
              //  list = _agent.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var agent in list)
            {
                agentModel = new AgentModel(agent);
                Modellist.Add(agentModel);
            }

            return Modellist;
        }

        /// <summary>
        /// AgentModel UpdateAgent(Agent agent):更新企业微信应用Agent
        /// </summary>
        /// <param name="agent">企业微信应用Agent实体</param>
        /// <returns>AgentModel</returns>
        public bool UpdateAgent(Agent agent)
        {
            var agentDB = _agent.All.FirstOrDefault(r => r.Id == agent.Id);
            if (agentDB != null)
            {
                agent.CreatedOn = agentDB.CreatedOn;
            //    AgentModel agentModel = new AgentModel(agent);
                _agent.Update(agent);
            }
            
            return agentDB != null;
        }
    }
}
