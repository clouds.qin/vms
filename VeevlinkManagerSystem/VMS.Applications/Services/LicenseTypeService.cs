﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class LicenseTypeService : ILicenseTypeService
    {
        private IRepository<LicenseType> _licenseType;
        public LicenseTypeService(IRepository<LicenseType> licenseType)
        {
            _licenseType = licenseType;
        }

        public LicenseTypeModel AddLicenseType(LicenseType licenseType)
        {
            _licenseType.Create(licenseType);
            LicenseTypeModel licenseTypeModel = new LicenseTypeModel(licenseType);
            return licenseTypeModel;
        }

        public bool DeleteLicenseType(string id)
        {
            var licenseType = _licenseType.All.FirstOrDefault(r => r.Id == id);
            if (licenseType != null)
            {
                _licenseType.Delete(licenseType);
                //license.Active = 0;
                UpdateLicenseType(licenseType);
            }

            return licenseType != null;
        }

        public List<LicenseTypeModel> GetLicenseType(string keyWord, int page)
        {
            LicenseTypeModel licenseTypeModel;
            var Modellist = new List<LicenseTypeModel>();
            int pageSize = 10;
            List<LicenseType> list = new List<LicenseType>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _licenseType.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _licenseType.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _licenseType.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _licenseType.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var licenseType in list)
            {
                licenseTypeModel = new LicenseTypeModel(licenseType);
                Modellist.Add(licenseTypeModel);
            }

            return Modellist;
        }

        public Boolean UpdateLicenseType(LicenseType licenseType)
        {
            var licenseTypeDB = _licenseType.All.FirstOrDefault(r => r.Id == licenseType.Id);
            if (licenseTypeDB != null)
            {
                licenseType.CreatedOn = licenseTypeDB.CreatedOn;
                // LicenseModel licenseModel = new LicenseModel(license);
                _licenseType.Update(licenseType);
            }
            return licenseTypeDB != null;
        }
    }
}
