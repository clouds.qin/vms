﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class WelinkUserService : IWelinkUserService
    {
        private IRepository<WelinkUser> _welinkUser;
        public WelinkUserService(IRepository<WelinkUser> welinkUser)
        {
            _welinkUser = welinkUser;
        }

        public WelinkUserModel AddWelinkUser(WelinkUser welinkUser)
        {
            _welinkUser.Create(welinkUser);
            WelinkUserModel welinkUserModel = new WelinkUserModel(welinkUser);
            return welinkUserModel;
        }

        public bool DeleteWelinkUser(string id)
        {
            var welinkUser = _welinkUser.All.FirstOrDefault(r => r.Id == id);
            if (welinkUser != null)
            {
                //_welinkUser.Delete(welinkUser);
                welinkUser.Active = 0;
                UpdateWelinkUser(welinkUser);
            }

            return welinkUser != null;
        }

        public List<WelinkUserModel> GetWelinkUser(string keyWord, int page)
        {
            WelinkUserModel welinkUserModel;
            var Modellist = new List<WelinkUserModel>();
            int pageSize = 10;
            List<WelinkUser> list = new List<WelinkUser>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _welinkUser.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _welinkUser.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _welinkUser.All.Where(p => p.NickName.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _welinkUser.All.Where(p => p.NickName.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var welinkUser in list)
            {
                welinkUserModel = new WelinkUserModel(welinkUser);
                Modellist.Add(welinkUserModel);
            }

            return Modellist;
        }

        public Boolean UpdateWelinkUser(WelinkUser welinkUser)
        {
            var welinkUserDB = _welinkUser.All.FirstOrDefault(r => r.Id == welinkUser.Id);
            if (welinkUserDB != null)
            {
                welinkUser.CreatedOn = welinkUserDB.CreatedOn;
                // WelinkUserModel welinkUserModel = new WelinkUserModel(welinkUser);
                _welinkUser.Update(welinkUser);
            }
            return welinkUserDB != null;
        }
    }
}


