﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class TemporaryRecordService : ITemporaryRecordService
    {
        private IRepository<TemporaryRecord> _temporaryRecord;
        public TemporaryRecordService(IRepository<TemporaryRecord> temporaryRecord)
        {
            _temporaryRecord = temporaryRecord;
        }

        public TemporaryRecordModel AddTemporaryRecord(TemporaryRecord temporaryRecord)
        {
            _temporaryRecord.Create(temporaryRecord);
            TemporaryRecordModel temporaryRecordModel = new TemporaryRecordModel(temporaryRecord);
            return temporaryRecordModel;
        }

        public bool DeleteTemporaryRecord(string id)
        {
            var temporaryRecord = _temporaryRecord.All.FirstOrDefault(r => r.Id == id);
            if (temporaryRecord != null)
            {
                _temporaryRecord.Delete(temporaryRecord);
                // synchronouedQueue.Active = 0;
                UpdateTemporaryRecord(temporaryRecord);
            }

            return temporaryRecord != null;
        }

        public List<TemporaryRecordModel> GetTemporaryRecord(string keyWord, int page)
        {
            TemporaryRecordModel temporaryRecordModel;
            var Modellist = new List<TemporaryRecordModel>();
            int pageSize = 10;
            List<TemporaryRecord> list = new List<TemporaryRecord>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _temporaryRecord.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _suite.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _temporaryRecord.All.Where(p => p.TableName.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _suite.All.Where(p => p.SuiteName.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var temporaryRecord in list)
            {
                temporaryRecordModel = new TemporaryRecordModel(temporaryRecord);
                Modellist.Add(temporaryRecordModel);
            }

            return Modellist;
        }

        public Boolean UpdateTemporaryRecord(TemporaryRecord temporaryRecord)
        {
            var temporaryRecordDB = _temporaryRecord.All.FirstOrDefault(r => r.Id == temporaryRecord.Id);
            if (temporaryRecordDB != null)
            {
                temporaryRecord.CreatedOn = temporaryRecordDB.CreatedOn;
                // TemporaryRecordModel temporaryRecordModel = new TemporaryRecordModel(temporaryRecord);
                _temporaryRecord.Update(temporaryRecord);
            }
            return temporaryRecordDB != null;
        }
    }
}


