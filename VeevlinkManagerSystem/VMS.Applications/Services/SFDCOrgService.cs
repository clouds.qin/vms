﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class SFDCOrgService : ISFDCOrgService
    {
        private IRepository<Agent> _agent;
        private IRepository<Organization> _organization;
        private IRepository<SFDCOrg> _sfdcOrg;
        private IRepository<WelinkAccount> _welinkAccount;
        public SFDCOrgService(IRepository<Agent> agent, IRepository<Organization> organization, IRepository<SFDCOrg> sfdcOrg, IRepository<WelinkAccount> welinkAccount)
        {
            _agent = agent;
            _organization = organization;
            _sfdcOrg = sfdcOrg;
            _welinkAccount = welinkAccount;
        }

        public SFDCOrgModel AddSFDCOrg(SFDCOrg sfdcOrg)
        {
            _sfdcOrg.Create(sfdcOrg);
            SFDCOrgModel sfdcOrgModel = new SFDCOrgModel(sfdcOrg);
            return sfdcOrgModel;
        }

        public bool DeleteSFDCOrg(string id)
        {
            var sfdcOrg = _sfdcOrg.All.FirstOrDefault(r => r.Id == id);
            if (sfdcOrg != null)
            {
                // _sfdcOrg.Delete(sfdcOrg);
                sfdcOrg.Active = 0;
                UpdateSFDCOrg(sfdcOrg);
            }

            return sfdcOrg != null;
        }

        public List<SFDCOrgModel> GetSFDCOrg(string keyWord, int page)
        {
            SFDCOrgModel sfdcOrgModel;
            var Modellist = new List<SFDCOrgModel>();
            int pageSize = 10;
            List<SFDCOrg> list = new List<SFDCOrg>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _sfdcOrg.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _sfdcOrg.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _sfdcOrg.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _sfdcOrg.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var sfdcOrg in list)
            {
                sfdcOrgModel = new SFDCOrgModel(sfdcOrg);
                Modellist.Add(sfdcOrgModel);
            }

            return Modellist;
        }

        public bool UpdateSFDCOrg(SFDCOrg sfdcOrg)
        {
            var sfdcOrgDB = _sfdcOrg.All.FirstOrDefault(r => r.Id == sfdcOrg.Id);
            if (sfdcOrgDB != null)
            {
                sfdcOrg.CreatedOn = sfdcOrgDB.CreatedOn;
                //SFDCOrgModel sfdcOrgModel = new SFDCOrgModel(sfdcOrg);
                _sfdcOrg.Update(sfdcOrg);
            }
           
            return sfdcOrgDB != null;
        }
    }
}
