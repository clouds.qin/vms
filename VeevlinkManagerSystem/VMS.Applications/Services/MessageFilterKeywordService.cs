﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class MessageFilterKeywordService : IMessageFilterKeywordService
    {
        private IRepository<MessageFilterKeyword> _messageFilterKeyword;
        public MessageFilterKeywordService(IRepository<MessageFilterKeyword> messageFilterKeyword)
        {
            _messageFilterKeyword = messageFilterKeyword;
        }

        public MessageFilterKeywordModel AddMessageFilterKeyword(MessageFilterKeyword messageFilterKeyword)
        {
            _messageFilterKeyword.Create(messageFilterKeyword);
            MessageFilterKeywordModel messageFilterKeywordModel = new MessageFilterKeywordModel(messageFilterKeyword);
            return messageFilterKeywordModel;
        }

        public bool DeleteMessageFilterKeyword(string id)
        {
            var messageFilterKeyword = _messageFilterKeyword.All.FirstOrDefault(r => r.Id == id);
            if (messageFilterKeyword != null)
            {
                //messageFilterKeyword.Delete(messageFilter);
                messageFilterKeyword.Active = 0;
                UpdateMessageFilterKeyword(messageFilterKeyword);
            }

            return messageFilterKeyword != null;
        }

        public List<MessageFilterKeywordModel> GetMessageFilterKeyword(string keyWord, int page)
        {
            MessageFilterKeywordModel messageFilterKeywordModel;
            var Modellist = new List<MessageFilterKeywordModel>();
            int pageSize = 10;
            List<MessageFilterKeyword> list = new List<MessageFilterKeyword>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _messageFilterKeyword.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _messageFilterKeyword.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _messageFilterKeyword.All.Where(p => p.Keyword.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _messageFilterKeyword.All.Where(p => p.Keyword.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var messageFilterKeyword in list)
            {
                messageFilterKeywordModel = new MessageFilterKeywordModel(messageFilterKeyword);
                Modellist.Add(messageFilterKeywordModel);
            }

            return Modellist;
        }

        public Boolean UpdateMessageFilterKeyword(MessageFilterKeyword messageFilterKeyword)
        {
            var messageFilterDB = _messageFilterKeyword.All.FirstOrDefault(r => r.Id == messageFilterKeyword.Id);
            if (messageFilterDB != null)
            {
                messageFilterKeyword.CreatedOn = messageFilterDB.CreatedOn;
                // MessageFilterKeywordModel messageFilterKeywordModel = new MessageFilterKeywordModel(messageFilterKeyword);
                _messageFilterKeyword.Update(messageFilterKeyword);
            }
            return messageFilterDB != null;
        }
    }
}
