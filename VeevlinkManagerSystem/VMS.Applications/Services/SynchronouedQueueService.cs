﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class SynchronouedQueueService : ISynchronouedQueueService
    {
        private IRepository<SynchronouedQueue> _synchronouedQueue;
        public SynchronouedQueueService(IRepository<SynchronouedQueue> synchronouedQueue)
        {
            _synchronouedQueue = synchronouedQueue;
        }

        public SynchronouedQueueModel AddSynchronouedQueue(SynchronouedQueue synchronouedQueue)
        {
            _synchronouedQueue.Create(synchronouedQueue);
            SynchronouedQueueModel synchronouedQueueModel = new SynchronouedQueueModel(synchronouedQueue);
            return synchronouedQueueModel;
        }

        public bool DeleteSynchronouedQueue(string id)
        {
            var synchronouedQueue = _synchronouedQueue.All.FirstOrDefault(r => r.Id == id);
            if (synchronouedQueue != null)
            {
                _synchronouedQueue.Delete(synchronouedQueue);
                // synchronouedQueue.Active = 0;
                UpdateSynchronouedQueue(synchronouedQueue);
            }

            return synchronouedQueue != null;
        }

        public List<SynchronouedQueueModel> GetSynchronouedQueue(string keyWord, int page)
        {
            SynchronouedQueueModel synchronouedQueueModel;
            var Modellist = new List<SynchronouedQueueModel>();
            int pageSize = 10;
            List<SynchronouedQueue> list = new List<SynchronouedQueue>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _synchronouedQueue.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _suite.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _synchronouedQueue.All.Where(p => p.TableName.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _suite.All.Where(p => p.SuiteName.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var synchronouedQueue in list)
            {
                synchronouedQueueModel = new SynchronouedQueueModel(synchronouedQueue);
                Modellist.Add(synchronouedQueueModel);
            }

            return Modellist;
        }

        public Boolean UpdateSynchronouedQueue(SynchronouedQueue synchronouedQueue)
        {
            var synchronouedQueueDB = _synchronouedQueue.All.FirstOrDefault(r => r.Id == synchronouedQueue.Id);
            if (synchronouedQueueDB != null)
            {
                synchronouedQueue.CreatedOn = synchronouedQueueDB.CreatedOn;
                // SynchronouedQueueModel synchronouedQueueModel = new SynchronouedQueue(suite);
                _synchronouedQueue.Update(synchronouedQueue);
            }
            return synchronouedQueueDB != null;
        }
    }
}


