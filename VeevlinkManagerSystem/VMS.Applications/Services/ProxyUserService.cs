﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class ProxyUserService : IProxyUserService
    {
        private IRepository<ProxyUser> _proxyUser;
        public ProxyUserService(IRepository<ProxyUser> proxyUser)
        {
            _proxyUser = proxyUser;
        }

        public ProxyUserModel AddProxyUser(ProxyUser proxyUser)
        {
            _proxyUser.Create(proxyUser);
            ProxyUserModel proxyUserModel = new ProxyUserModel(proxyUser);
            return proxyUserModel;
        }

        public bool DeletProxyUser(string id)
        {
            var proxyUser = _proxyUser.All.FirstOrDefault(r => r.Id == id);
            if (proxyUser != null)
            {
                //messageFilterKeyword.Delete(messageFilter);
                proxyUser.Active = 0;
                UpdateProxyUser(proxyUser);
            }

            return proxyUser != null;
        }

        public List<ProxyUserModel> GetProxyUser(string date, int page)
        {
            ProxyUserModel proxyUserModel;
            var Modellist = new List<ProxyUserModel>();
            int pageSize = 10;
            List<ProxyUser> list = new List<ProxyUser>();

            if (String.IsNullOrEmpty(date))
            {
                list = _proxyUser.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _proxyUser.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                DateTime dt = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
                var endTime = dt.AddDays(1);
                list = _proxyUser.All.Where(p => p.CreatedOn >= dt && p.CreatedOn< endTime).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _proxyUser.All.Where(p => p.RefreshToken.Contains(date)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _proxyUser.All.Where(p => p.Keyword.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var proxyUser in list)
            {
                proxyUserModel = new ProxyUserModel(proxyUser);
                Modellist.Add(proxyUserModel);
            }

            return Modellist;
        }

        public Boolean UpdateProxyUser(ProxyUser proxyUser)
        {
            var proxyUserDB = _proxyUser.All.FirstOrDefault(r => r.Id == proxyUser.Id);
            if (proxyUserDB != null)
            {
                proxyUser.CreatedOn = proxyUserDB.CreatedOn;
                // ProxyUserModel proxyUserModel = new ProxyUserModel(proxyUser);
                _proxyUser.Update(proxyUser);
            }
            return proxyUserDB != null;
        }
    }
}
