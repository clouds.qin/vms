﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class ComponentService : IComponentService
    {
        private IRepository<Component> _component;
        public ComponentService(IRepository<Component> component)
        {
            _component = component;
        }

        public ComponentModel AddComponent(Component component)
        {
            _component.Create(component);
            ComponentModel componentModel = new ComponentModel(component);
            return componentModel;
        }

        public bool DeleteComponent(string id)
        {
            var component = _component.All.FirstOrDefault(r => r.Id == id);
            if (component != null)
            {
                //_component.Delete(component);
                component.Active = 0;
                UpdateComponent(component);
            }

            return component != null;
        }

        public List<ComponentModel> GetComponent(string keyWord, int page)
        {
            ComponentModel componentModel;
            var Modellist = new List<ComponentModel>();
            int pageSize = 10;
            List<Component> list = new List<Component>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _component.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _component.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _component.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _component.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var component in list)
            {
                componentModel = new ComponentModel(component);
                Modellist.Add(componentModel);
            }

            return Modellist;
        }

        public Boolean UpdateComponent(Component component)
        {
            var componentDB = _component.All.FirstOrDefault(r => r.Id == component.Id);
            if (componentDB != null)
            {
                component.CreatedOn = componentDB.CreatedOn;
                // ComponentModel componentModel = new ComponentModel(component);
                _component.Update(component);
            }

            return componentDB != null;
        }
    }
}

