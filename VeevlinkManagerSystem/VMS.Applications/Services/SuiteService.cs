﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class SuiteService : ISuiteService
    {
        private IRepository<Suite> _suite;
        public SuiteService(IRepository<Suite> suite)
        {
            _suite = suite;
        }

        public SuiteModel Addsuite(Suite suite)
        {
            _suite.Create(suite);
            SuiteModel suiteModel = new SuiteModel(suite);
            return suiteModel;
        }

        public bool Deletesuite(string id)
        {
            var suite = _suite.All.FirstOrDefault(r => r.Id == id);
            if (suite != null)
            {
                _suite.Delete(suite);
               // suite.Active = 0;
                Updatesuite(suite);
            }

            return suite != null;
        }

        public List<SuiteModel> Getsuite(string keyWord, int page)
        {
            SuiteModel suiteModel;
            var Modellist = new List<SuiteModel>();
            int pageSize = 10;
            List<Suite> list = new List<Suite>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _suite.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _suite.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _suite.All.Where(p => p.SuiteName.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _suite.All.Where(p => p.SuiteName.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var suite in list)
            {
                suiteModel = new SuiteModel(suite);
                Modellist.Add(suiteModel);
            }

            return Modellist;
        }

        public Boolean Updatesuite(Suite suite)
        {
            var suiteDB = _suite.All.FirstOrDefault(r => r.Id == suite.Id);
            if (suiteDB != null)
            {
                suite.CreatedOn = suiteDB.CreatedOn;
                // SuiteModel suiteModel = new SuiteModel(suite);
                _suite.Update(suite);
            }
            return suiteDB != null;
        }
    }
}

