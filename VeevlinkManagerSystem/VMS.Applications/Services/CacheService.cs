﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class CacheService : ICacheService
    {
        private IRepository<Cache> _cache;
        public CacheService(IRepository<Cache> cache)
        {
            _cache = cache;
        }

        public CacheModel AddCache(Cache cache)
        {
            _cache.Create(cache);
            CacheModel cacheModel = new CacheModel(cache);
            return cacheModel;
        }

        public bool DeleteCache(string id)
        {
            var cache = _cache.All.FirstOrDefault(r => r.Id == id);
            if (cache != null)
            {
                _cache.Delete(cache);
                //cache.Active = 0;
                UpdateCache(cache);
            }

            return cache != null;
        }

        public List<CacheModel> GetCache(string keyWord, int page)
        {
            CacheModel cacheModel;
            var Modellist = new List<CacheModel>();
            int pageSize = 10;
            List<Cache> list = new List<Cache>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _cache.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _welinkAccount.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _cache.All.Where(p => p.Key.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _welinkAccount.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var cache in list)
            {
                cacheModel = new CacheModel(cache);
                Modellist.Add(cacheModel);
            }

            return Modellist;
        }

        public Boolean UpdateCache(Cache cache)
        {
            var cacheDB = _cache.All.FirstOrDefault(r => r.Id == cache.Id);
            if (cacheDB != null)
            {
                cache.CreatedOn = cacheDB.CreatedOn;
               // CacheModel cacheModel = new CacheModel(cache);
                _cache.Update(cache);
            }

            return cacheDB != null;
        }
    }
}

