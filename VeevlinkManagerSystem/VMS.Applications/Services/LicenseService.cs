﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Applications.IServices;
using VMS.Models;
using VMS.Repository;

namespace VMS.Applications.Services
{
    public class LicenseService : ILicenseService
    {
        private IRepository<License> _license;
        public LicenseService(IRepository<License> license)
        {
            _license = license;
        }

        public LicenseModel AddLicense(License license)
        {
            _license.Create(license);
            LicenseModel licenseModel = new LicenseModel(license);
            return licenseModel;
        }

        public bool DeleteLicense(string id)
        {
            var license = _license.All.FirstOrDefault(r => r.Id == id);
            if (license != null)
            {
                _license.Delete(license);
                //license.Active = 0;
                UpdateLicense(license);
            }

            return license != null;
        }

        public List<LicenseModel> GetLicense(string keyWord, int page)
        {
            LicenseModel licenseModel;
            var Modellist = new List<LicenseModel>();
            int pageSize = 10;
            List<License> list = new List<License>();

            if (String.IsNullOrEmpty(keyWord))
            {
                list = _license.All.OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                //list = _component.All.Where(p => p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                list = _license.All.Where(p => p.Name.Contains(keyWord)).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                // list = _component.All.Where(p => p.Name.Contains(keyWord) && p.Active != 0).OrderByDescending(p => p.CreatedOn).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var license in list)
            {
                licenseModel = new LicenseModel(license);
                Modellist.Add(licenseModel);
            }

            return Modellist;
        }

        public Boolean UpdateLicense(License license)
        {
            var licenseDB = _license.All.FirstOrDefault(r => r.Id == license.Id);
            if (licenseDB != null)
            {
                license.CreatedOn = licenseDB.CreatedOn;
                // LicenseModel licenseModel = new LicenseModel(license);
                _license.Update(license);
            }
            return licenseDB != null;
        }
    }
}
