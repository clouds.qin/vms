﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Repository
{
    [Table("Agent")]
    public class Agent : IEntity
    {
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string Name { get; set; }//非空
        public string Token { get; set; }//非空
        public string EncodingAESKey { get; set; }//非空
        public string AgentID { get; set; }//非空
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }//非空
        public string Secret { get; set; }
        public string AccessToken { get; set; }
        public DateTime? SyncOn { get; set; }

    }
    [Table("Organization")]
    public class Organization : IEntity
    {
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        public DateTime? SyncOn { get; set; }
    }
    [Table("SFDCOrg")]
    public class SFDCOrg : IEntity
    {
        public string Id { get; set; }//主键
        public string OrganizationId { get; set; }//非空
        public string Name { get; set; }//非空
        public string SFDCOrgID { get; set; }
        public string SFDCUserID { get; set; }
        public string Type { get; set; }
        public string PublicSiteURL { get; set; }
        public string InstanceURL { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? IssuedAt { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string LiveAgentURL { get; set; }
        public string LiveAgentDeploymentID { get; set; }
        public string LiveAgentButtonID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        public string NameSpace { get; set; }
        public string ReverseProxyUrl { get; set; }
        public DateTime? SyncOn { get; set; }
    }
    [Table("WelinkAccount")]
    public class WelinkAccount : IEntity
    {
        public string Id { get; set; }//主键
        public string OrganizationId { get; set; }//非空
        public string Name { get; set; }//非空
        public System.Byte Active { get; set; }//非空
        public System.Byte IsVeevlink { get; set; }
        public string LinkedSFDCOrgId { get; set; }//非空
        public string Type { get; set; }//非空
        public string SubType { get; set; }//非空
        public string WechatAccountName { get; set; }
        public string WechatAccountID { get; set; }
        public string WechatAccessToken { get; set; }
        public string IdentityMode { get; set; }//非空
        public string Channel { get; set; }
        public string AppID { get; set; }
        public string SourceID { get; set; }//非空
        public string AppSecret { get; set; }
        public DateTime? ExpiresIn { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string EncodingAESKey { get; set; }
        public string Token { get; set; }
        public string LiveAgentButtonID { get; set; }
        public string LiveAgentDeploymentID { get; set; }
        public string CalloutAdapter { get; set; }
        public string CallinAdapter { get; set; }
        public string LiveAgentRouter { get; set; }
        public string LiveAgentChasitorInit { get; set; }
        public string LiveAgentAssembly { get; set; }
        public string SendMessageAdapter { get; set; }
        public string MainAssembly { get; set; }
        public string CustomSetting { get; set; }
        public string JSAPITicket { get; set; }
        public System.Byte EnableJSAPI { get; set; }
        public string SFDCUserId { get; set; }
        public string SFDCAccessToken { get; set; }
        public string SFDCRefreshToken { get; set; }
        public string AuthorizerRefreshToken { get; set; }
        public string AuthorizerAccessToken { get; set; }
        public string ComponentId { get; set; }
        public string AuthorizationCode { get; set; }
        public string AuthorizationState { get; set; }
        public System.Byte EnableLiveChat { get; set; }
        public string ConnectionType { get; set; }
        public string IVRRule { get; set; }
        public System.Byte EnableReverseProxy { get; set; }
        public System.Byte ShowDevError { get; set; }
        public System.Byte EnableCommunity { get; set; }
        public string BackEndSystemType { get; set; }
        public string LiveAgentQueueUrl { get; set; }
        public string BaseAdapter { get; set; }
        public System.Byte EnableExternalQueue { get; set; }
        public int VeevChatQueueThreadNumber { get; set; }
        public int HandlerQueueThreadNumber { get; set; }
        public System.Byte EnableMessageQueue { get; set; }
        public System.Byte EnableVeevChat { get; set; }
        public string VeevChatQueueURL { get; set; }
        public string HandlerQueueURL { get; set; }
        public DateTime? Syncon { get; set; }  
    }

    [Table("Cache")]
    public class Cache : IEntity
    {
        public string Id { get; set; }//主键
        public string WelinkUserId { get; set; }//非空
        public string Key { get; set; }
        public string Value { get; set; }
        public System.Byte Overdue { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    [Table("Component")]
    public class Component : IEntity
    {
        public string Id { get; set; }//主键
        public string Name { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string VerifyTicket { get; set; }
        public string AccessToken { get; set; }
        public string Token { get; set; }
        public string EncodingAESKey { get; set; }
        public System.Byte Active { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public DateTime? SyncOn { get; set; }
    }
    
    [Table("License")]
    public class License : IEntity
    {
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        public string OrganizationId { get; set; }//非空
        public string LicenseTypeId { get; set; }//非空
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    [Table("LicenseType")]
    public class LicenseType : IEntity
    {
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    [Table("MessageFilter")]
    public class MessageFilter : IEntity
    {
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string MessageType { get; set; }
        public System.Byte AsynForward { get; set; }
        public System.Byte Active { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public DateTime? SyncOn { get; set; }
        public string EventType { get; set; }
    }

   [Table("MessageFilterKeyword")]
    public class MessageFilterKeyword : IEntity
    {
        public string Id { get; set; }//主键
        public string MessageFilterId { get; set; }//非空
        public string Keyword { get; set; }
        public System.Byte Active { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public DateTime? SyncOn { get; set; }
    }
    
    [Table("ProxyUser")]
    public class ProxyUser : IEntity
    {
        public string Id { get; set; }//主键
        public string SFDCOrgId { get; set; }//非空
        public string WelinkAccountId { get; set; }//非空
        public string SFDCUserID { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public System.Byte Active { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public DateTime? SyncOn { get; set; }
    }

    [Table("Suite")]
    public class Suite : IEntity
    {
        public string Id { get; set; }//主键
        public string SuiteId { get; set; }//非空
        public string SuiteSecret { get; set; }//非空
        public string SuiteName { get; set; }//非空
        public string Token { get; set; }//非空
        public string EncodingAESKey { get; set; }//非空
        public string SuiteTicket { get; set; }
        public string CallBackURL { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    [Table("SynchronouedQueue")]
    public class SynchronouedQueue : IEntity
    {
        public string Id { get; set; }//非空
        public string RecordId { get; set; }
        public string TableName { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

    }

    [Table("TemporaryRecord")]
    public class TemporaryRecord : IEntity
    {
        public string Id { get; set; }//非空
        public string TableName { get; set; }
        public string RecordId { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    [Table("WelinkAdoption")]
    public class WelinkAdoption : IEntity
    {
        public string Id { get; set; }//主键
        public string SFDCOrgId { get; set; }//非空
        public string OrganizationId { get; set; }//非空
        public string WelinkAccountId { get; set; }//非空
        public string WelinkUserId { get; set; }//非空
        public string AgentId { get; set; }
        public string MsgType { get; set; }
        public string EventType { get; set; }
        public string EventKey { get; set; }
        public string Action { get; set; }
        public string ActionType { get; set; }
        public DateTime? ActionTime { get; set; }
        public string FromUserID { get; set; }
        public string ToUserID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Type { get; set; }
        public string Content { get; set; }
        public string MsgId { get; set; }
        public string MediaId { get; set; }
        public string MsgBody { get; set; }
        public System.Byte IsVeevlink { get; set; }
        public System.Byte IsLiveAgent { get; set; }
        public string SFDCResponse { get; set; }
    }
    
    [Table("WelinkUser")]
    public class WelinkUser : IEntity
    {
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string WechatUserID { get; set; }
        public string SFDCUserID { get; set; }
        public string SFDCAccessToken { get; set; }
        public string SFDCRefreshToken { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Language { get; set; }
        public string Mobile { get; set; }
        public string NickName { get; set; }
        public string OpenID { get; set; }
        public string Position { get; set; }
        public string Province { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public DateTime? SubscribeTime { get; set; }
        public string UnionID { get; set; }
        public string UniqueID { get; set; }
        public DateTime? UnsubscribeTime { get; set; }
        public string WechatID { get; set; }
        public string Type { get; set; }
        public string Subscribe { get; set; }
        public string Name { get; set; }
        public string ProxyUserId { get; set; }
        public DateTime? Syncon { get; set; }
    }
}