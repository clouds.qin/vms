﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace VMS.Repository
{
    public interface IEntity
    {
    }
    public interface IAttribute
    {
        T GetAttribute<T>();
    }
    public interface IAttributeID
    {
        string AttributeID { get; }
    }
}
