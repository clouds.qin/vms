﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Repository
{
    public class EFDBContext : DbContext, IRepositoryContext
    {
        public EFDBContext() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //对表名称 取消s 
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Agent> AgentEntity { get; set; }
        public DbSet<Organization> OrganizationEntity { get; set; }
        public DbSet<SFDCOrg> SFDCOrgEntity { get; set; }
        public DbSet<WelinkAccount> WelinkAccountEntity { get; set; }
        public DbSet<Cache> CacheEntity { get; set; }
        public DbSet<Component> ComponentEntity { get; set; }
        public DbSet<License> LicenseEntity { get; set; }
        public DbSet<LicenseType> LicenseTypeEntity { get; set; }
        public DbSet<MessageFilter> MessageFilterEntity { get; set; }
        public DbSet<MessageFilterKeyword> MessageFilterKeywordEntity { get; set; }
        public DbSet<ProxyUser> ProxyUserEntity { get; set; }
        public DbSet<Suite> SuiteEntity { get; set; }
        public DbSet<SynchronouedQueue> SynchronouedQueueEntity { get; set; }
        public DbSet<TemporaryRecord> TemporaryRecordEntity { get; set; }
        public DbSet<WelinkAdoption> WelinkAdoptionEntity { get; set; }
        public DbSet<WelinkUser> WelinkUserEntity { get; set; }
    }
}
