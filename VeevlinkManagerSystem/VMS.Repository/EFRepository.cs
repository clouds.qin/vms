﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Repository
{

    public class EFRepository<TEntity> : RepositoryBase<TEntity> where TEntity : class
    {
        public EFDBContext DBcontext { get; private set; }
        public EFRepository(IRepositoryContext context) : base(context)
        {
            DBcontext = (EFDBContext)context;

        }

        public override IQueryable<TEntity> All
        {
            get
            {
                return DBcontext.Set<TEntity>().AsNoTracking().AsQueryable<TEntity>();
            }
        }

        public override void Create(TEntity entity)
        {
            try
            {
                if (!DBcontext.Set<TEntity>().Local.Contains(entity))
                {
                    DBcontext.Set<TEntity>().Attach(entity);
                }
                DBcontext.Set<TEntity>().Add(entity);
                DBcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public override void Delete(TEntity entity)
        {
            //return;
            try
            {
                if (!DBcontext.Set<TEntity>().Local.Contains(entity))
                {
                    DBcontext.Set<TEntity>().Attach(entity);
                }
                DBcontext.Set<TEntity>().Remove(entity);
                DBcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override void Delete(int Id)
        {
            //return;
            var entity = DBcontext.Set<TEntity>().Find(Id);
            if (!DBcontext.Set<TEntity>().Local.Contains(entity))
            {
                DBcontext.Set<TEntity>().Attach(entity);
            }
            //  DBcontext.Set<TEntity>().Attach(entity);
            DBcontext.Set<TEntity>().Remove(entity);
            DBcontext.SaveChanges();
        }

        public override void Delete(params Expression<Func<TEntity, bool>>[] predicates)
        {
           // return;
            try
            {
                IQueryable<TEntity> query = All;
                foreach (var predicate in predicates)
                {
                    query = query.Where(predicate);
                }
                var entities = query.ToList();
                foreach (var entity in entities)
                {
                    DBcontext.Set<TEntity>().Remove(entity);
                }
                DBcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Delete(IList<TEntity> entities)
        {
            //return;
            foreach (var item in entities)
            {
                try
                {
                    DBcontext.Set<TEntity>().Remove(item);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            DBcontext.SaveChanges();
        }

        public override bool Exist(params Expression<Func<TEntity, bool>>[] predicates)
        {
            try
            {
                IQueryable<TEntity> query = All;
                foreach (var predicate in predicates)
                {
                    query = query.Where(predicate);
                }
                return query.Count() > 0;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        public override TEntity Retrive<TKey>(TKey key)
        {
            try
            {
                return DBcontext.Set<TEntity>().Find(key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public override IList<TEntity> Retrive(IList<TKey> keys)
        //{
        //    throw new NotImplementedException();
        //}

        public override void Update(TEntity entity)
        {
            try
            {
                DBcontext.Set<TEntity>().Attach(entity);
                DBcontext.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
                DBcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Update(IList<TEntity> entities)
        {
            throw new NotImplementedException();
        }
    }
}
