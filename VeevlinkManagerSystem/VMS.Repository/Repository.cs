﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace VMS.Repository
{
    public interface IRepository<TEntity>
    {
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int Id);
        void Delete(IList<TEntity> entities);
        void Delete(params Expression<Func<TEntity, Boolean>>[] predicates);
        void Update(TEntity entity);
        void Update(IList<TEntity> entities);
        Boolean Exist(params Expression<Func<TEntity, Boolean>>[] predicates);
        IQueryable<TEntity> All { get; }
    }


    public abstract class RepositoryBase<TEntity> : IRepository<TEntity>
    {

        public RepositoryBase(IRepositoryContext context)
        {
            Context = context;
        }

        public IRepositoryContext Context { get; private set; }
        public abstract void Create(TEntity entity);
        public abstract void Delete(TEntity entity);
        public abstract void Delete(params Expression<Func<TEntity, Boolean>>[] predicates);
        public abstract void Delete(IList<TEntity> entities);
        public abstract void Update(TEntity entity);
        public abstract void Update(IList<TEntity> entities);
        public abstract Boolean Exist(params Expression<Func<TEntity, Boolean>>[] predicates);
        public abstract TEntity Retrive<TKey>(TKey key);
        public abstract void Delete(int Id);


        public abstract IQueryable<TEntity> All { get; }
    }
}
