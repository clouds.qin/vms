﻿using Newtonsoft.Json;
using System;
using VMS.Repository;

namespace VMS.Models
{
    public class AgentModel
    {
        public AgentModel()
        { }
        public AgentModel(Agent agent)
        {
            this.Id = agent.Id;
            this.WelinkAccountId = agent.WelinkAccountId;
            this.Name = agent.Name;
            this.Token = agent.Token;
            this.EncodingAESKey = agent.EncodingAESKey;
            this.AgentID = agent.AgentID;
            this.CreatedOn = agent.CreatedOn.ToString();
            this.LastModifiedOn = agent.LastModifiedOn.ToString();
            this.Active = agent.Active;
            this.Secret = agent.Secret;
            this.AccessToken = agent.AccessToken;
            this.SyncOnM = agent.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
        }
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string Name { get; set; }//非空
        public string Token { get; set; }//非空
        public string EncodingAESKey { get; set; }//非空
        public string AgentID { get; set; }//非空
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }//非空
        public string Secret { get; set; }
        public string AccessToken { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
    }

    public class OrganizationModel
    {
        public OrganizationModel() { }
        public OrganizationModel(Organization organization)
        {
            this.Id = organization.Id;
            this.Name = organization.Name;
            this.Active = organization.Active;
            this.SyncOnM = organization.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
            this.CreatedOn = organization.CreatedOn.ToString();
            this.LastModifiedOn = organization.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
    }

    public class SFDCOrgModel
    {
        public SFDCOrgModel()
        { }
        public SFDCOrgModel(SFDCOrg sfdcOrg)
        {
            this.Id = sfdcOrg.Id;
            this.OrganizationId = sfdcOrg.OrganizationId;
            this.Name = sfdcOrg.Name;
            this.SFDCOrgID = sfdcOrg.SFDCOrgID;
            this.SFDCUserID = sfdcOrg.SFDCUserID;
            this.Type = sfdcOrg.Type;
            this.PublicSiteURL = sfdcOrg.PublicSiteURL;
            this.InstanceURL = sfdcOrg.InstanceURL;
            this.AccessToken = sfdcOrg.AccessToken;
            this.RefreshToken = sfdcOrg.RefreshToken;
            this.IssuedAtM = sfdcOrg.IssuedAt;
            this.IssuedAt = this.IssuedAtM.ToString();
            this.ClientID = sfdcOrg.ClientID;
            this.ClientSecret = sfdcOrg.ClientSecret;
            this.LiveAgentURL = sfdcOrg.LiveAgentURL;
            this.LiveAgentDeploymentID = sfdcOrg.LiveAgentDeploymentID;
            this.LiveAgentButtonID = sfdcOrg.LiveAgentButtonID;
            this.CreatedOn = sfdcOrg.CreatedOn.ToString();
            this.LastModifiedOn = sfdcOrg.LastModifiedOn.ToString();
            this.Active = sfdcOrg.Active;
            this.NameSpace = sfdcOrg.NameSpace;
            this.ReverseProxyUrl = sfdcOrg.ReverseProxyUrl;
            this.SyncOnM = sfdcOrg.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
        }
        public string Id { get; set; }//主键
        public string OrganizationId { get; set; }//非空
        public string Name { get; set; }//非空
        public string SFDCOrgID { get; set; }
        public string SFDCUserID { get; set; }
        public string Type { get; set; }
        public string PublicSiteURL { get; set; }
        public string InstanceURL { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? IssuedAtM { get; set; }
        public string IssuedAt { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string LiveAgentURL { get; set; }
        public string LiveAgentDeploymentID { get; set; }
        public string LiveAgentButtonID { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        public string NameSpace { get; set; }
        public string ReverseProxyUrl { get; set; }
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }

    }

    public class WelinkAccountModel
    {
        public WelinkAccountModel()
        { }
        public WelinkAccountModel(WelinkAccount welinkAccount)
        {
            this.Id = welinkAccount.Id;
            this.OrganizationId = welinkAccount.OrganizationId;
            this.Name = welinkAccount.Name;
            this.Active = welinkAccount.Active;
            this.IsVeevlink = welinkAccount.IsVeevlink;
            this.LinkedSFDCOrgId = welinkAccount.LinkedSFDCOrgId;
            this.Type = welinkAccount.Type;
            this.SubType = welinkAccount.SubType;
            this.WechatAccountName = welinkAccount.WechatAccountName;
            this.WechatAccountID = welinkAccount.WechatAccountID;
            this.WechatAccessToken = welinkAccount.WechatAccessToken;
            this.IdentityMode = welinkAccount.IdentityMode;
            this.Channel = welinkAccount.Channel;
            this.AppID = welinkAccount.AppID;
            this.SourceID = welinkAccount.SourceID;
            this.AppSecret = welinkAccount.AppSecret;
            this.ExpiresInM = welinkAccount.ExpiresIn;
            this.ExpiresIn = this.ExpiresInM.ToString();
            this.CreatedOn = welinkAccount.CreatedOn.ToString();
            this.LastModifiedOn = welinkAccount.LastModifiedOn.ToString();
            this.EncodingAESKey = welinkAccount.EncodingAESKey;
            this.Token = welinkAccount.Token;
            this.LiveAgentButtonID = welinkAccount.LiveAgentButtonID;
            this.CalloutAdapter = welinkAccount.CalloutAdapter;
            this.CallinAdapter = welinkAccount.CallinAdapter;
            this.LiveAgentRouter = welinkAccount.LiveAgentRouter;
            this.LiveAgentChasitorInit = welinkAccount.LiveAgentChasitorInit;
            this.LiveAgentAssembly = welinkAccount.LiveAgentAssembly;
            this.SendMessageAdapter = welinkAccount.SendMessageAdapter;
            this.MainAssembly = welinkAccount.MainAssembly;
            this.CustomSetting = welinkAccount.CustomSetting;
            this.JSAPITicket = welinkAccount.JSAPITicket;
            this.EnableJSAPI = welinkAccount.EnableJSAPI;
            this.SFDCRefreshToken = welinkAccount.SFDCRefreshToken;
            this.SFDCUserId = welinkAccount.SFDCUserId;
            this.SFDCAccessToken = welinkAccount.SFDCAccessToken;
            this.AuthorizerRefreshToken = welinkAccount.AuthorizerRefreshToken;
            this.AuthorizerAccessToken = welinkAccount.AuthorizerAccessToken;
            this.ComponentId = welinkAccount.ComponentId;
            this.AuthorizationCode = welinkAccount.AuthorizationCode;
            this.AuthorizationState = welinkAccount.AuthorizationState;
            this.EnableLiveChat = welinkAccount.EnableLiveChat;
            this.ConnectionType = welinkAccount.ConnectionType;
            this.IVRRule = welinkAccount.IVRRule;
            this.EnableReverseProxy = welinkAccount.EnableReverseProxy;
            this.ShowDevError = welinkAccount.ShowDevError;
            this.EnableCommunity = welinkAccount.EnableCommunity;
            this.BackEndSystemType = welinkAccount.BackEndSystemType;
            this.LiveAgentQueueUrl = welinkAccount.LiveAgentQueueUrl;
            this.BaseAdapter = welinkAccount.BaseAdapter;
            this.EnableExternalQueue = welinkAccount.EnableExternalQueue;
            this.VeevChatQueueThreadNumber = welinkAccount.VeevChatQueueThreadNumber;
            this.HandlerQueueThreadNumber = welinkAccount.HandlerQueueThreadNumber;
            this.EnableMessageQueue = welinkAccount.EnableMessageQueue;
            this.EnableVeevChat = welinkAccount.EnableVeevChat;
            this.VeevChatQueueURL = welinkAccount.VeevChatQueueURL;
            this.HandlerQueueURL = welinkAccount.HandlerQueueURL;
            this.SynconM = welinkAccount.Syncon;
            this.Syncon = this.SynconM.ToString();
        }
        public string Id { get; set; }//主键
        public string OrganizationId { get; set; }//非空
        public string Name { get; set; }//非空
        public System.Byte Active { get; set; }//非空
        public System.Byte IsVeevlink { get; set; }
        public string LinkedSFDCOrgId { get; set; }//非空
        public string Type { get; set; }//非空
        public string SubType { get; set; }//非空
        public string WechatAccountName { get; set; }
        public string WechatAccountID { get; set; }
        public string WechatAccessToken { get; set; }
        public string IdentityMode { get; set; }//非空
        public string Channel { get; set; }
        public string AppID { get; set; }
        public string SourceID { get; set; }//非空
        public string AppSecret { get; set; }
        [JsonIgnore]
        public DateTime? ExpiresInM { get; set; }
        public string ExpiresIn { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public string EncodingAESKey { get; set; }
        public string Token { get; set; }
        public string LiveAgentButtonID { get; set; }
        public string LiveAgentDeploymentID { get; set; }
        public string CalloutAdapter { get; set; }
        public string CallinAdapter { get; set; }
        public string LiveAgentRouter { get; set; }
        public string LiveAgentChasitorInit { get; set; }
        public string LiveAgentAssembly { get; set; }
        public string SendMessageAdapter { get; set; }
        public string MainAssembly { get; set; }
        public string CustomSetting { get; set; }
        public string JSAPITicket { get; set; }
        public System.Byte EnableJSAPI { get; set; }
        public string SFDCUserId { get; set; }
        public string SFDCAccessToken { get; set; }
        public string SFDCRefreshToken { get; set; }
        public string AuthorizerRefreshToken { get; set; }
        public string AuthorizerAccessToken { get; set; }
        public string ComponentId { get; set; }
        public string AuthorizationCode { get; set; }
        public string AuthorizationState { get; set; }
        public System.Byte EnableLiveChat { get; set; }
        public string ConnectionType { get; set; }
        public string IVRRule { get; set; }
        public System.Byte EnableReverseProxy { get; set; }
        public System.Byte ShowDevError { get; set; }
        public System.Byte EnableCommunity { get; set; }
        public string BackEndSystemType { get; set; }
        public string LiveAgentQueueUrl { get; set; }
        public string BaseAdapter { get; set; }
        public System.Byte EnableExternalQueue { get; set; }
        public int VeevChatQueueThreadNumber { get; set; }
        public int HandlerQueueThreadNumber { get; set; }
        public System.Byte EnableMessageQueue { get; set; }
        public System.Byte EnableVeevChat { get; set; }
        public string VeevChatQueueURL { get; set; }
        public string HandlerQueueURL { get; set; }
        [JsonIgnore]
        public DateTime? SynconM { get; set; }
        public string Syncon { get; set; }
    }

    public class CacheModel
    {
        public CacheModel() { }
        public CacheModel(Cache cache)
        {
            this.Id = cache.Id;
            this.WelinkUserId = cache.WelinkUserId;
            this.Key = cache.Key;
            this.Value = cache.Value;
            this.Overdue = cache.Overdue;
            this.CreatedOn = cache.CreatedOn.ToString();
            this.LastModifiedOn = cache.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//主键
        public string WelinkUserId { get; set; }//非空
        public string Key { get; set; }
        public string Value { get; set; }
        public System.Byte Overdue { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class ComponentModel
    {
        public ComponentModel() { }
        public ComponentModel(Component component)
        {
            this.Id = component.Id;
            this.Name = component.Name;
            this.AppId = component.AppId;
            this.AppSecret = component.AppSecret;
            this.VerifyTicket = component.VerifyTicket;
            this.AccessToken = component.AccessToken;
            this.Token = component.Token;
            this.EncodingAESKey = component.EncodingAESKey;
            this.Active = component.Active;
            this.CreatedOn = component.CreatedOn.ToString();
            this.LastModifiedOn = component.LastModifiedOn.ToString();
            this.SyncOnM = component.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
        }
        public string Id { get; set; }//主键
        public string Name { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string VerifyTicket { get; set; }
        public string AccessToken { get; set; }
        public string Token { get; set; }
        public string EncodingAESKey { get; set; }
        public System.Byte Active { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
    }

    public class LicenseModel
    {
        public LicenseModel() { }
        public LicenseModel(License license)
        {
            this.Id = license.Id;
            this.Name = license.Name;
            this.OrganizationId = license.OrganizationId;
            this.LicenseTypeId = license.LicenseTypeId;
            this.CreatedOn = license.CreatedOn.ToString();
            this.LastModifiedOn = license.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        public string OrganizationId { get; set; }//非空
        public string LicenseTypeId { get; set; }//非空
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class LicenseTypeModel
    {
        public LicenseTypeModel() { }
        public LicenseTypeModel(LicenseType licenseType)
        {
            this.Id = licenseType.Id;
            this.Name = licenseType.Name;
            this.CreatedOn = licenseType.CreatedOn.ToString();
            this.LastModifiedOn = licenseType.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//主键
        public string Name { get; set; }//非空
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class MessageFilterModel
    {
        public MessageFilterModel() { }
        public MessageFilterModel(MessageFilter messageFilter)
        {
            this.Id = messageFilter.Id;
            this.WelinkAccountId = messageFilter.WelinkAccountId;
            this.MessageType = messageFilter.MessageType;
            this.AsynForward = messageFilter.AsynForward;
            this.Active = messageFilter.Active;
            this.CreatedOn = messageFilter.CreatedOn.ToString();
            this.LastModifiedOn = messageFilter.LastModifiedOn.ToString();
            this.SyncOnM = messageFilter.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
            this.EventType = messageFilter.EventType;
        }
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string MessageType { get; set; }
        public System.Byte AsynForward { get; set; }
        public System.Byte Active { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
        public string EventType { get; set; }
    }

    public class MessageFilterKeywordModel
    {
        public MessageFilterKeywordModel() { }
        public MessageFilterKeywordModel(MessageFilterKeyword messageFilterKeyword)
        {
            this.Id = messageFilterKeyword.Id;
            this.MessageFilterId = messageFilterKeyword.MessageFilterId;
            this.Keyword = messageFilterKeyword.Keyword;
            this.Active = messageFilterKeyword.Active;
            this.CreatedOn = messageFilterKeyword.CreatedOn.ToString();
            this.LastModifiedOn = messageFilterKeyword.LastModifiedOn.ToString();
            this.SyncOnM = messageFilterKeyword.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
        }
        public string Id { get; set; }//主键
        public string MessageFilterId { get; set; }//非空
        public string Keyword { get; set; }
        public System.Byte Active { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
    }

    public class ProxyUserModel
    {
        public ProxyUserModel() { }
        public ProxyUserModel(ProxyUser proxyUser)
        {
            this.Id = proxyUser.Id;
            this.SFDCOrgId = proxyUser.SFDCOrgId;
            this.WelinkAccountId = proxyUser.WelinkAccountId;
            this.SFDCUserID = proxyUser.SFDCUserID;
            this.AccessToken = proxyUser.AccessToken;
            this.RefreshToken = proxyUser.RefreshToken;
            this.Active = proxyUser.Active;
            this.CreatedOn = proxyUser.CreatedOn.ToString();
            this.LastModifiedOn = proxyUser.LastModifiedOn.ToString();
            this.SyncOnM = proxyUser.SyncOn;
            this.SyncOn = this.SyncOnM.ToString();
        }
        public string Id { get; set; }//主键
        public string SFDCOrgId { get; set; }//非空
        public string WelinkAccountId { get; set; }//非空
        public string SFDCUserID { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public System.Byte Active { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        [JsonIgnore]
        public DateTime? SyncOnM { get; set; }
        public string SyncOn { get; set; }
    }

    public class SuiteModel
    {
        public SuiteModel() { }
        public SuiteModel(Suite suite)
        {
            this.Id = suite.Id;
            this.SuiteId = suite.SuiteId;
            this.SuiteSecret = suite.SuiteSecret;
            this.SuiteName = suite.SuiteName;
            this.Token = suite.Token;
            this.EncodingAESKey = suite.EncodingAESKey;
            this.SuiteTicket = suite.SuiteTicket;
            this.CallBackURL = suite.CallBackURL;
            this.CreatedOn = suite.CreatedOn.ToString();
            this.LastModifiedOn = suite.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//主键
        public string SuiteId { get; set; }//非空
        public string SuiteSecret { get; set; }//非空
        public string SuiteName { get; set; }//非空
        public string Token { get; set; }//非空
        public string EncodingAESKey { get; set; }//非空
        public string SuiteTicket { get; set; }
        public string CallBackURL { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class SynchronouedQueueModel
    {
        public SynchronouedQueueModel() { }
        public SynchronouedQueueModel(SynchronouedQueue synchronouedQueue)
        {
            this.Id = synchronouedQueue.Id;
            this.RecordId = synchronouedQueue.RecordId;
            this.TableName = synchronouedQueue.TableName;
            this.Type = synchronouedQueue.Type;
            this.CreatedOn = synchronouedQueue.CreatedOn.ToString();
            this.LastModifiedOn = synchronouedQueue.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//非空
        public string RecordId { get; set; }
        public string TableName { get; set; }
        public string Type { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class TemporaryRecordModel
    {
        public TemporaryRecordModel() { }
        public TemporaryRecordModel(TemporaryRecord temporaryRecord)
        {
            this.Id = temporaryRecord.Id;
            this.RecordId = temporaryRecord.RecordId;
            this.TableName = temporaryRecord.TableName;
            this.Type = temporaryRecord.Type;
            this.CreatedOn = temporaryRecord.CreatedOn.ToString();
            this.LastModifiedOn = temporaryRecord.LastModifiedOn.ToString();
        }
        public string Id { get; set; }//非空
        public string TableName { get; set; }
        public string RecordId { get; set; }
        public string Type { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
    }

    public class WelinkAdoptionModel
    {
        public WelinkAdoptionModel() { }
        public WelinkAdoptionModel(WelinkAdoption welinkAdoption)
        {
            this.Id = welinkAdoption.Id;
            this.SFDCOrgId = welinkAdoption.SFDCOrgId;
            this.OrganizationId = welinkAdoption.OrganizationId;
            this.WelinkAccountId = welinkAdoption.WelinkAccountId;
            this.WelinkUserId = welinkAdoption.WelinkUserId;
            this.AgentId = welinkAdoption.AgentId;
            this.EventType = welinkAdoption.EventType;
            this.MsgType = welinkAdoption.MsgType;
            this.EventKey = welinkAdoption.EventKey;
            this.Action = welinkAdoption.Action;
            this.ActionType = welinkAdoption.ActionType;
            this.ActionTimeM = welinkAdoption.ActionTime;
            this.ActionTime = this.ActionTimeM.ToString();
            this.FromUserID = welinkAdoption.FromUserID;
            this.ToUserID = welinkAdoption.ToUserID;
            this.CreatedOn = welinkAdoption.CreatedOn.ToString();
            this.Type = welinkAdoption.Type;
            this.Content = welinkAdoption.Content;
            this.MsgId = welinkAdoption.MsgId;
            this.MediaId = welinkAdoption.MediaId;
            this.MsgBody = welinkAdoption.MsgBody;
            this.IsVeevlink = welinkAdoption.IsVeevlink;
            this.IsLiveAgent = welinkAdoption.IsLiveAgent;
            this.SFDCResponse = welinkAdoption.SFDCResponse;
        }
        public string Id { get; set; }//主键
        public string SFDCOrgId { get; set; }//非空
        public string OrganizationId { get; set; }//非空
        public string WelinkAccountId { get; set; }//非空
        public string WelinkUserId { get; set; }//非空
        public string AgentId { get; set; }
        public string MsgType { get; set; }
        public string EventType { get; set; }
        public string EventKey { get; set; }
        public string Action { get; set; }
        public string ActionType { get; set; }
        [JsonIgnore]
        public DateTime? ActionTimeM { get; set; }
        public string ActionTime { get; set; }

        public string FromUserID { get; set; }
        public string ToUserID { get; set; }
        //public DateTime? CreatedOn { get; set; }
        public string CreatedOn { get; set; }
        public string Type { get; set; }
        public string Content { get; set; }
        public string MsgId { get; set; }
        public string MediaId { get; set; }
        public string MsgBody { get; set; }
        public System.Byte IsVeevlink { get; set; }
        public System.Byte IsLiveAgent { get; set; }
        public string SFDCResponse { get; set; }
    }

    public class WelinkUserModel
    {
        public WelinkUserModel() { }
        public WelinkUserModel(WelinkUser welinkUser)
        {
            this.Id = welinkUser.Id;
            this.WelinkAccountId = welinkUser.WelinkAccountId;
            this.WechatUserID = welinkUser.WechatUserID;
            this.SFDCUserID = welinkUser.SFDCUserID;
            this.SFDCAccessToken = welinkUser.SFDCAccessToken;
            this.SFDCRefreshToken = welinkUser.SFDCRefreshToken;
            this.CreatedOn = welinkUser.CreatedOn.ToString();
            this.LastModifiedOn = welinkUser.LastModifiedOn.ToString();
            this.Active = welinkUser.Active;
            this.City = welinkUser.City;
            this.Country = welinkUser.Country;
            this.Email = welinkUser.Email;
            this.Language = welinkUser.Language;
            this.Mobile = welinkUser.Mobile;
            this.NickName = welinkUser.NickName;
            this.OpenID = welinkUser.OpenID;
            this.Position = welinkUser.Position;
            this.Province = welinkUser.Province;
            this.Sex = welinkUser.Sex;
            this.Status = welinkUser.Status;
            this.SubscribeTimeM = welinkUser.SubscribeTime;
            this.SubscribeTime = this.SubscribeTimeM.ToString();

            this.UnionID = welinkUser.UnionID;
            this.UniqueID = welinkUser.UniqueID;
            this.UnsubscribeTimeM = welinkUser.UnsubscribeTime;
            this.UnsubscribeTime = this.UnsubscribeTimeM.ToString();
            this.WechatID = welinkUser.WechatID;
            this.Type = welinkUser.Type;
            this.Subscribe = welinkUser.Subscribe;
            this.Name = welinkUser.Name;
            this.ProxyUserId = welinkUser.ProxyUserId;
            this.SynconM = welinkUser.Syncon;
            this.Syncon = this.SynconM.ToString();
        }
        public string Id { get; set; }//主键
        public string WelinkAccountId { get; set; }//非空
        public string WechatUserID { get; set; }
        public string SFDCUserID { get; set; }
        public string SFDCAccessToken { get; set; }
        public string SFDCRefreshToken { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public DateTime? LastModifiedOn { get; set; }
        public string CreatedOn { get; set; }
        public string LastModifiedOn { get; set; }
        public System.Byte Active { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Language { get; set; }
        public string Mobile { get; set; }
        public string NickName { get; set; }
        public string OpenID { get; set; }
        public string Position { get; set; }
        public string Province { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        [JsonIgnore]
        public DateTime? SubscribeTimeM { get; set; }
        public string SubscribeTime { get; set; }
        public string UnionID { get; set; }
        public string UniqueID { get; set; }
        [JsonIgnore]
        public DateTime? UnsubscribeTimeM { get; set; }
        public string UnsubscribeTime { get; set; }
        public string WechatID { get; set; }
        public string Type { get; set; }
        public string Subscribe { get; set; }
        public string Name { get; set; }
        public string ProxyUserId { get; set; }
        [JsonIgnore]
        public DateTime? SynconM { get; set; }
        public string Syncon { get; set; }
    }
}
